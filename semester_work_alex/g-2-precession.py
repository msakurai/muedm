import ROOT as rt
import numpy as np

rt.gStyle.SetOptStat(0)
rt.gROOT.SetBatch(1)

run_numbers = [10, 100, 200, 300]
labels = ["pixelanalysis"] * 4
use_exact_vertex = True  # whether to use vertex from root file or positron tracking

a = 11659206 * 10 ** -10  # (g-2)/2  (from PDG)
B = 3
omega = 1.602e-19/1.883531475e-28 *a*B
T = 2 * np.pi / omega
print("omega_{g-2}",T, "s")

amplitudes = []
amplitude_errs = []

for i in range(len(run_numbers)):
    run_number = run_numbers[i]  # run without E-field
    label = labels[i]

    if label != "":
        label = "_" + label
    raw_data_file = 'raw_data/musr_' + str(run_number) + '_.root'
    processed_data_file = 'processed_data/pixel_data_' + str(run_number) + label + '.root'

    if __name__ == '__main__':
        input_file = rt.TFile(processed_data_file, 'READ')
        tree = input_file.Get("px0nm")
        tree.GetEntry(0)

        # import data tree
        t1 = rt.TChain('t1')
        t1.Add(raw_data_file)

        t_bin = 500
        t_min = 0
        t_max = 20

        canvas = rt.TCanvas("canvas", "Asymmetry Plot - g-2 Precession", 1000, 500)
        canvas.Divide(1, 3)

        out_histogram = rt.TH1F("out_histogram", "", t_bin, t_min, t_max)
        in_histogram = rt.TH1F("in_histogram", "", t_bin, t_min, t_max)
        out_histogram.Sumw2()
        in_histogram.Sumw2()

        if not use_exact_vertex:
            tree.Draw("time >> out_histogram", "radial_direction > 0.5 && vertical_direction != 0", "gOff")
            tree.Draw("time >> in_histogram", "radial_direction < -0.5 && vertical_direction != 0", "gOff")
        else:
            t1.Draw("muDecayTime >> out_histogram", "muDecayPosX * posIniMomX + muDecayPosY * posIniMomY  > 0", "gOff")
            t1.Draw("muDecayTime >> in_histogram", "muDecayPosX * posIniMomX + muDecayPosY * posIniMomY  < 0", "gOff")

        out_in_diff = out_histogram.Clone("out_in_diff")
        out_in_diff.Add(in_histogram, -1)

        out_in_sum = out_histogram.Clone("out_in_sum")
        out_in_sum.Add(in_histogram)

        asymmetry_histogram = rt.TH1F("asymmetry_histogram", "Asymmetry Plot; Time (#mus); Out-In Asymmetry",
                                      t_bin, t_min, t_max)
        asymmetry_histogram = out_in_diff.Clone("asymmetry_histogram")
        asymmetry_histogram.GetYaxis().SetRangeUser(-0.5, 0.5)
        asymmetry_histogram.Divide(out_in_sum)

        func = rt.TF1('func', '[0]*sin([1]*x+[2]) + [3]', 0, 25)
        func.SetParNames('A_{g-2}', '#omega_{g-2}', '#phi_{g-2}', 'const')
        func.SetParameters(-0.178, 3, 0.1, 0)
        asymmetry_histogram.Fit('func', 'REMQ')

        # draw the histograms
        canvas.cd(1)
        out_histogram.Draw()
        canvas.cd(2)
        in_histogram.Draw()
        canvas.cd(3)
        asymmetry_histogram.Draw()
        canvas.Draw()

        tree.GetEntry(0)
        asymmetry_histogram.SetTitle("Asymmetry Plot; Time (#mus); Out-In Asymmetry")
        legend = rt.TLegend(0.7, 0.65, 0.9, 0.9)
        legend.SetHeader("pixel size = %.3f mm" % (tree.pixel_size), "C")
        legend.AddEntry(func, 'y = A_{g-2} sin(#omega_{g-2}x + #phi_{g-2})')
        legend.AddEntry(func, 'A_{g-2} = %.3f #pm %.3f' % (func.GetParameter(0), func.GetParError(0)))
        legend.AddEntry(func, '#omega_{g-2} = %.3f #pm %.3f' % (func.GetParameter(1), func.GetParError(1)))
        legend.AddEntry(func, '#phi_{g-2} = %.3f #pm %.3f' % (func.GetParameter(2), func.GetParError(2)))
        legend.Draw()

        # filename label to include the flag use_exact_vertex flag
        exact_label = "_tracking"
        if use_exact_vertex:
            exact_label = "_exact"
        canvas.SaveAs("graphs/asymmetry_plot_g-2-precession_" + str(run_number) + exact_label + ".png")

        precession_freq = func.GetParameter("#omega_{g-2}") * 10 ** 6  # s^-1
        amplitude = func.GetParameter("A_{g-2}")

        print('A_{g-2} = %.3f #pm %.3f' % (func.GetParameter(0), func.GetParError(0)))
        amplitudes.append(func.GetParameter(0))
        amplitude_errs.append(func.GetParError(0))

print(amplitudes)
print(amplitude_errs)