import ROOT as rt
import numpy as np

InputFile = rt.TFile('pixeltrees.root', 'READ')
keylist = InputFile.GetListOfKeys()

for key in keylist:
    tree_name = key.GetName()
    my_tree = InputFile.Get(tree_name)
    my_tree.Scan()

