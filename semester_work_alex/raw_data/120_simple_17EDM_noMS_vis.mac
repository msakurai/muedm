# Set the EDM limit
/musr/command edmLimit 1.8 -17

###################################################################################
#############################  VOLUME DEFINITIONS  ################################
###################################################################################

# VOLUME SYNTAX:
# /musr/command construct solid name x_len y_len z_len material x y z motherVolume matrixName sensitiveClass idNumber


# DEFINE THE WORLD VOLUME
/musr/command construct box World 200 200 200 VacuumCustom1 0 0 0 no_logical_volume norot dead -1

# DEFINE A ROTATION MATRIX
/musr/command rotation detector_rotation 0 0 0 

# Central Cylinder Detectors
/musr/command construct tubs innerinner_detector 70 70.1 300 0 360 VacuumCustom1 0 0 0 log_World detector_rotation musr/ScintSD 10
/musr/command construct tubs inner_detector 110 110.1 300 0 360 VacuumCustom1 0 0 0 log_World detector_rotation musr/ScintSD 11
/musr/command construct tubs outer_detector 120 120.1 300 0 360 VacuumCustom1 0 0 0 log_World detector_rotation musr/ScintSD 12

# Graphite Electrodes
/musr/command construct tubs outer_electrode 150 150.05 55 0 360 VacuumCustom1 0 0 0 log_World detector_rotation dead 30
/musr/command construct tubs inner_electrode 130 130.05 55 0 360 VacuumCustom1 0 0 0 log_World detector_rotation dead 40


###################################################################################
##############################  FIELD DEFINITIONS  ################################
###################################################################################

# DEFINE A MAGNETIC FIELD
/musr/command globalfield uniform_vertical_field 1000 1000 1000 uniform 140 0 0 log_World 0 0 -3 0 0 0

# 0.962427634970251 radial electric field at 1.5T, rescaled for 3T
/musr/command globalfield E_field 150 150 150 radial  0 0 0 log_World 0 0 0 1.92485526994 0 0

# focussing field
/musr/command globalfield  focus_field 0 0 0 fromfile 2DB focus_field.txt log_World -1

# Set parameters for particle tracking in an EM field
/musr/command signalSeparationTime 0.01
/musr/command globalfield setparameter SetLargestAcceptableStep 5
/musr/command globalfield setparameter SetMinimumEpsilonStep 5e-5
/musr/command globalfield setparameter SetMaximumEpsilonStep 0.001
/musr/command globalfield setparameter SetDeltaOneStep 0.1
/musr/command globalfield setparameter SetDeltaIntersection 0.01
# /musr/command globalfield printparameters

/musr/command globalfield printFieldValueAtPoint 140 0 0
/musr/command globalfield printFieldValueAtPoint 140 0 50


###################################################################################
#########################  P H Y S I C S      P R O C E S S E S  ##################
###################################################################################
# Geant 4.9.4
/musr/command process addDiscreteProcess gamma G4PhotoElectricEffect
/musr/command process addDiscreteProcess gamma G4ComptonScattering
/musr/command process addDiscreteProcess gamma G4GammaConversion
/musr/command process addDiscreteProcess gamma G4RayleighScattering
/musr/command process addProcess         e-    G4eMultipleScattering    -1  1  1
/musr/command process addProcess         e-    G4eIonisation            -1  2  2
/musr/command process addProcess         e-    G4eBremsstrahlung        -1  3  3
/musr/command process addProcess         e+    G4eMultipleScattering    -1  1  1
/musr/command process addProcess         e+    G4eIonisation            -1  2  2
/musr/command process addProcess         e+    G4eBremsstrahlung        -1  3  3
/musr/command process addProcess         e+    G4eplusAnnihilation       0 -1  4
#/musr/command process addProcess         mu-   G4MuMultipleScattering   -1  1  1
#/musr/command process addProcess         mu-   G4MuIonisation           -1  2  2
#/musr/command process addProcess         mu-   G4MuBremsstrahlung       -1  3  3
#/musr/command process addProcess         mu-   G4MuPairProduction       -1  4  4
#/musr/command process addProcess         mu+   G4MuMultipleScattering   -1  1  1
#/musr/command process addProcess         mu+   G4MuIonisation           -1  2  2
#/musr/command process addProcess         mu+   G4MuBremsstrahlung       -1  3  3
#/musr/command process addProcess         mu+   G4MuPairProduction       -1  4  4

###################################################################################
##############################   OTHER OPTIONS  ###################################
###################################################################################

/musr/run/howOftenToPrintEvent 1000

/musr/command rootOutputDirectoryName trash
/musr/command storeOnlyEventsWithHits false
/musr/command storeOnlyTheFirstTimeHit false
#/musr/command maximumRunTimeAllowed
/musr/command rootOutput step_info off


###################################################################################
#########################   R O O T     O U T P U T  ##############################
###################################################################################
/musr/command rootOutput eventID on
/musr/command rootOutput posIniMomX on
/musr/command rootOutput posIniMomY on
/musr/command rootOutput posIniMomZ on
/musr/command rootOutput det_ID on
/musr/command rootOutput det_start on
/musr/command rootOutput det_end on
/musr/command rootOutput det_x on
/musr/command rootOutput det_y on
/musr/command rootOutput det_z on
/musr/command rootOutput muDecayPosX on
/musr/command rootOutput muDecayPosY on
/musr/command rootOutput muDecayPosZ on
/musr/command rootOutput muDecayTime on
/musr/command rootOutput muDecayPolX on
/musr/command rootOutput muDecayPolY on
/musr/command rootOutput muDecayPolZ on

/musr/command rootOutput runID off
/musr/command rootOutput weight off
/musr/command rootOutput BFieldAtDecay off
/musr/command rootOutput muIniPosX off
/musr/command rootOutput muIniPosY off
/musr/command rootOutput muIniPosZ off
/musr/command rootOutput muIniMomX off
/musr/command rootOutput muIniMomY off
/musr/command rootOutput muIniMomZ off
/musr/command rootOutput muIniPolX off
/musr/command rootOutput muIniPolY off
/musr/command rootOutput muIniPolZ off
/musr/command rootOutput muIniTime off
/musr/command rootOutput muDecayDetID off


/musr/command rootOutput muTargetTime off
/musr/command rootOutput muTargetPolX off
/musr/command rootOutput muTargetPolY off
/musr/command rootOutput muTargetPolZ off
/musr/command rootOutput muM0Time off
/musr/command rootOutput muM0PolX off
/musr/command rootOutput muM0PolY off
/musr/command rootOutput muM0PolZ off
/musr/command rootOutput muM1Time off
/musr/command rootOutput muM1PolX off
/musr/command rootOutput muM1PolY off
/musr/command rootOutput muM1PolZ off
/musr/command rootOutput muM2Time off
/musr/command rootOutput muM2PolX off
/musr/command rootOutput muM2PolY off
/musr/command rootOutput muM2PolZ off
/musr/command rootOutput fieldNomVal off
/musr/command rootOutput det_edep off
/musr/command rootOutput det_edep_el off
/musr/command rootOutput det_edep_pos off
/musr/command rootOutput det_edep_gam off
/musr/command rootOutput det_edep_mup off
/musr/command rootOutput det_nsteps off
/musr/command rootOutput det_length off
/musr/command rootOutput det_kine off
/musr/command rootOutput det_VrtxKine off
/musr/command rootOutput det_VrtxX off
/musr/command rootOutput det_VrtxY off
/musr/command rootOutput det_VrtxZ off
/musr/command rootOutput det_VrtxVolID off
/musr/command rootOutput det_VrtxProcID off
/musr/command rootOutput det_VrtxTrackID off
/musr/command rootOutput det_VrtxParticleID off
/musr/command rootOutput det_VvvKine off
/musr/command rootOutput det_VvvX off
/musr/command rootOutput det_VvvY off
/musr/command rootOutput det_VvvZ off
/musr/command rootOutput det_VvvVolID off
/musr/command rootOutput det_VvvProcID off
/musr/command rootOutput det_VvvTrackID off
/musr/command rootOutput det_VvvParticleID off
/musr/command rootOutput odet_ID           off
/musr/command rootOutput nOptPhot          off
/musr/command rootOutput odet_nPhot        off
/musr/command rootOutput odet_timeFirst    off
/musr/command rootOutput odet_timeA        off
/musr/command rootOutput odet_timeB        off
/musr/command rootOutput odet_timeC        off
/musr/command rootOutput odet_timeD        off
/musr/command rootOutput odet_timeE        off
/musr/command rootOutput odet_timeMean     off
/musr/command rootOutput odet_timeLast     off
/musr/command rootOutput odet_timeCFD      off
/musr/command rootOutput odet_amplCFD      off


###################################################################################
#########################  V I S U A L I S A T I O N ##############################
###################################################################################
# DEFINE COLORS FOR VOLUMES
/musr/command visattributes G4_GRAPHITE grey
/musr/command visattributes G4_PLASTIC_SC_VINYLTOLUENE blue
/musr/command visattributes log_World invisible
#/musr/command visattributes G4_PLASTIC_SC_VINYLTOLUENE SCINT_style



#/vis/disable 				# uncomment this to disable visualisation
/control/execute visVRML.mac		# comment this to disable visualisation

# Other visualisation options
#/control/execute visFromToni.mac
#/control/execute visDawn101.mac  


###################################################################################
###############################  PARTICLE GUN  ####################################
###################################################################################

/gun/primaryparticle mu+
/gun/vertex 140 0 0 mm
/gun/momentum 125 MeV
/gun/direction 0 1.0 0
/gun/muonPolarizVector 0 1.0 0

# DIVERGENCE
/gun/momentumsmearing 0.3 MeV
/gun/tiltsigma 0.1 0.1 0.1 deg
/gun/vertexsigma 2 10 2 mm

###################################################################################
####################  RESTRICTIONS TO IMPROVE PERFORMANCE  ########################
###################################################################################
# force muons to decay at 30 us, since we only plot before 25 us
/gun/decaytimelimits −1 30000  2197.03 ns

# kill irrelevant particles
/musr/command killAllElectrons true
/musr/command killAllGammas true
/musr/command killAllNeutrinos false

# limits track length of positrons to 20 m (~23 revolutions at 14 cm)
# prevents positrons from getting trapped in weakly focussing field
/musr/command process addProcess e+ G4UserSpecialCuts -1 -1 5
/musr/command SetUserLimits log_World -1 20000 0 0 0

/vis/viewer/flush
/run/beamOn 5
