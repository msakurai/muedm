import ROOT as rt
import numpy as np
from utils.tree_tools import add_branch2tree, get_list_of_treenames

rt.gStyle.SetOptStat(0)
rt.gROOT.SetBatch(1)

save_asymmetry_graphs = True
use_exact_vertex = False  # whether to use vertex from root file or positron tracking

run_numbers = [12, 120, 220, 320, 11, 110, 210, 310]
modes =  ["sin"] * 4 + ["linear"] * 4
labels = ["0px"] * 8
actual_EDMs = [1.8e-17] * 4 + [1.8e-18] * 4
amplitudes = []
amplitude_errs = []
g2_amplitudes = [-0.16120106276441312, -0.1568710021503209, -0.16753088491936519, -0.1603795338023088]
g2_amplitude_errs = [0.004595237214185509, 0.004596223425909344, 0.004600245254345345, 0.004594724631545971]

muEDMs = []
muEDMuncertainties = []
muEDMerrors = []
for i in range(len(run_numbers)):
    run_number = run_numbers[i]
    label = labels[i]

    mode = modes[i]  # linear or sin
    actual_EDM = actual_EDMs[i]

    if i >= 4:
        A = np.abs(g2_amplitudes[i-4])
        A_err = g2_amplitude_errs[i-4]
    else:
        A = 0.180
        A_err = 0.003

    if label != "":
        label = "_" + label
    raw_data_file = 'raw_data/musr_' + str(run_number) + '_.root'
    processed_data_file = 'processed_data/pixel_data_' + str(run_number) + label + '.root'

    # import processed data trees
    input_file = rt.TFile(processed_data_file, 'READ')
    treenames = get_list_of_treenames(processed_data_file)
    print("trees in root file: ", treenames)
    tree = input_file.Get("px0nm")

    # import raw data tree
    t1 = rt.TChain('t1')
    t1.Add(raw_data_file)

    if __name__ == '__main__':
        tmin = 0  # lower time limit for histogram, us
        tmax = 25  # upper time limit for histogram, us
        tbin = 50  # number of bins

        canvas1 = rt.TCanvas("canvas1", "Asymmetry Plot", 1000,500)
        up_histogram = rt.TH1F("up_histogram","", tbin, tmin, tmax)
        down_histogram = rt.TH1F("down_histogram","", tbin, tmin, tmax)
        up_histogram.Sumw2()
        down_histogram.Sumw2()
        if use_exact_vertex:
            t1.Draw("muDecayTime >> up_histogram", "posIniMomZ > 0", "gOff")
            t1.Draw("muDecayTime >> down_histogram", "posIniMomZ < 0", "gOff")
        else:
            tree.Draw("time >> up_histogram", "vertical_direction > 0.5", "gOff")
            tree.Draw("time >> down_histogram", "vertical_direction < -0.5", "gOff")

        up_down_diff = up_histogram.Clone("up_down_diff")
        up_down_diff.Add(down_histogram, -1)

        up_down_sum = up_histogram.Clone("up_down_sum")
        up_down_sum.Add(down_histogram)

        asymmetry_histogram = up_down_diff.Clone("asymmetry_histogram")
        asymmetry_histogram.Divide(up_down_sum)
        asymmetry_histogram.SetTitle("Asymmetry Plot; Time (#mus); Up-Down Asymmetry")
        asymmetry_histogram.GetYaxis().SetRangeUser(-0.5, 0.5)

        # tree.GetEntry(0)
        legend = rt.TLegend(0.7,0.65,0.9,0.9)

        # Compute the muon EDM
        hbar = 6.582119569 * 10 ** -16  # eV * s
        m_mu = 105.6583755  # MeV
        p_mu = 125  # MeV
        gamma = np.sqrt(m_mu ** 2 + p_mu ** 2) / m_mu  # dimensionless

        E_f = 1.92485526994  # keV/mm
        a = 11659206 * 10 ** -10  # (g-2)/2  (from PDG)
        B = 3
        omega = 2*actual_EDM / hbar * E_f/a/gamma**2 * 1e4  # s^-1
        T = 2 * np.pi / omega
        print("precession freq theoretical:",T*1e6,"microsecond")
        if mode == "linear":
            func = rt.TF1('func', '[0] * x + [1]', 0, 20)

            func.SetParNames('m', 'c')
            func.SetParameters(1, 0)
            asymmetry_histogram.Fit('func', 'REMQ')
            legend.AddEntry(func,'y = m*x + c')
            legend.AddEntry(func,'m= %.3f #pm %.3f'%(func.GetParameter(0), func.GetParError(0)))
            legend.AddEntry(func,'c = %.3f #pm %.3f'%(func.GetParameter(1), func.GetParError(1)))
            P_0 = 1  # initial polarization
            slope = func.GetParameter(0) * 10 ** 6  # s^-1
            slope_err = func.GetParError(0) * 10 ** 6  # s^-1

            beta = np.sqrt(1-1/gamma**2)
            B = 3  # T
            precession_freq = slope/P_0/A
            precession_freq_err = precession_freq * np.sqrt((slope_err/slope)**2 + (A_err/A)**2)
        else:
            func = rt.TF1('func', '[0]*sin([1]*x)', 0, 25)
            func.SetParNames('A_{edm}', '#omega_{e}', '#phi_{e}')
            func.SetParameters(0.2, 0.38, 1)
            asymmetry_histogram.Draw()

            asymmetry_histogram.Fit('func', 'REMQ')

            # tree.GetEntry(0)
            asymmetry_histogram.SetTitle("Asymmetry Plot; Time (#mus); Up-Down Asymmetry")
            legend = rt.TLegend(0.7, 0.65, 0.9, 0.9)

            legend.AddEntry(func, 'y = A_{edm} sin(#omega_{e}x + #phi_{e})')
            legend.AddEntry(func, 'A_{edm} = %.3f #pm %.3f' % (func.GetParameter(0), func.GetParError(0)))
            legend.AddEntry(func, '#omega_{e} = %.3f #pm %.3f' % (func.GetParameter(1), func.GetParError(1)))
            legend.AddEntry(func, '#phi_{e} = %.3f #pm %.3f' % (func.GetParameter(2), func.GetParError(2)))

            amplitudes.append(func.GetParameter("A_{edm}"))
            amplitude_errs.append(func.GetParError(0))


            precession_freq = func.GetParameter(1) * 10 ** 6  # s^-1
            precession_freq_err = func.GetParError(1) * 10 ** 6  # s^-1

        mu_EDM = precession_freq * hbar / 2. * a * gamma**2 / E_f  # s**-1 * eV * s / (keV/mm)
        mu_EDM_uncertainty = mu_EDM * precession_freq_err/precession_freq
        canvas1.Draw()
        legend.Draw()

        tracking_label = "_tracking"
        if use_exact_vertex:
            tracking_label = "_exact"
        if save_asymmetry_graphs:
            canvas1.SaveAs("graphs/asymmetry_plot_calcEDM_" + str(run_number) + tracking_label + ".png")



        mu_EDM = mu_EDM / 10000  # cm
        mu_EDM_uncertainty = mu_EDM_uncertainty / 10000
        mu_EDM_error = (mu_EDM - actual_EDM) / (actual_EDM) * 100
        print("muon electric dipole moment: ", mu_EDM)
        print("uncertainty: ", mu_EDM_uncertainty/mu_EDM*100, "%")
        print("inaccuracy: ", mu_EDM_error, "%")

        muEDMs.append(mu_EDM)
        muEDMuncertainties.append(mu_EDM_uncertainty)
        muEDMerrors.append(mu_EDM_error)

        del canvas1
        del asymmetry_histogram
        del up_down_sum
        del up_down_diff
        del up_histogram
        del down_histogram
        del func
print("Results:")
print("run_number ", *run_numbers)
print("amplitudes ", *amplitudes)
print("error      ", *list(np.array(amplitude_errs) / np.array(amplitudes)*100), sep="  ")
print("muEDM      ", *muEDMs,sep="  ")
print("uncertainty", *list(np.array(muEDMuncertainties) / np.array(muEDMs)*100),sep="  ")
print("error     ", *muEDMerrors,sep="  ")