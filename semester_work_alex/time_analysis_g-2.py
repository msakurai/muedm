import ROOT as rt
import numpy as np
import matplotlib.pyplot as plt
from utils.tree_tools import add_branch2tree, get_list_of_treenames

rt.gStyle.SetOptStat(0)
rt.gROOT.SetBatch(1)

run_number = 200
label = "pixelanalysis"
use_exact_vertex = True   # specifies whether to use exact decay vertex or positron tracking
save_asymmetry_graphs = False

guess = -0.178, 3, -0.1, 0  # initial guess for fit, results may depend strongly on it!

if label != "":
    label = "_" + label

# label which specifies whether the helix fitting algorithm or the exact decay vertex was used.
# appended to the filename of the plots
exact_label = "tracking"
if use_exact_vertex:
    exact_label = "exact"

resolutions = 10**(np.linspace(-4,1.5,300))

processed_data_file = 'processed_data/pixel_data_' + str(run_number) + label + '.root'
raw_data_file = 'raw_data/musr_' + str(run_number) + '_.root'

# import processed data trees
input_file = rt.TFile(processed_data_file, 'READ')
treenames = get_list_of_treenames(processed_data_file)
print("trees in root file: ", treenames)
tree = input_file.Get("px0nm")

# import raw data tree
t1 = rt.TChain('t1')
t1.Add(raw_data_file)

if __name__ == '__main__':
    tmin = 0  # lower time limit for histogram, us
    tmax = 25  # upper time limit for histogram, us

    pixel_sizes = []
    a_s = []
    a_errs = []
    accuracies = []
    efficiencies = []
    tmom_average_errors = []
    vmom_average_errors = []
    biases = []

    for time_resolution in resolutions:
        tbin = int((tmax - tmin) / time_resolution)  # number of bins

        canvas1 = rt.TCanvas("canvas1", "Asymmetry Plot", 1000,500)
        up_histogram = rt.TH1F("out_histogram", "", tbin, tmin, tmax)
        down_histogram = rt.TH1F("in_histogram", "", tbin, tmin, tmax)
        up_histogram.Sumw2()
        down_histogram.Sumw2()

        if not use_exact_vertex:
            tree.Draw("time >> out_histogram", "radial_direction > 0.5 && vertical_direction != 0", "gOff")
            tree.Draw("time >> in_histogram", "radial_direction < -0.5 && vertical_direction != 0", "gOff")
        else:
            t1.Draw("det_time_start >> out_histogram", "muDecayPosX * posIniMomX + muDecayPosY * posIniMomY  > 0",
                    "gOff")
            t1.Draw("det_time_start >> in_histogram", "muDecayPosX * posIniMomX + muDecayPosY * posIniMomY  < 0",
                    "gOff")

        out_in_diff = up_histogram.Clone("out_in_diff")
        out_in_diff.Add(down_histogram, -1)

        out_in_sum = up_histogram.Clone("out_in_sum")
        out_in_sum.Add(down_histogram)

        asymmetry_histogram = out_in_diff.Clone("asymmetry_histogram")
        asymmetry_histogram.Divide(out_in_sum)
        asymmetry_histogram.SetTitle("Asymmetry Plot; Time (#mus); Up-Down Asymmetry")
        asymmetry_histogram.GetYaxis().SetRangeUser(-0.5, 0.5)
        asymmetry_histogram.Draw()

        # up_down_diff.Draw()


        # tree.GetEntry(0)
        legend = rt.TLegend(0.7,0.65,0.9,0.9)
        legend.SetHeader("time resolution = %.3f #mus" % (time_resolution), "C")

        func = rt.TF1('func', '[0]*sin([1]*x + [2])', 0, 25)
        func.SetParNames('A_{g-2}', '#omega_{g-2}', '#phi_{g-2}', 'const')
        func.SetParameters(*guess)

        asymmetry_histogram.Fit('func', 'REMQ')
        # tree.GetEntry(0)
        asymmetry_histogram.SetTitle("Asymmetry Plot; Time (#mus); Up-Down Asymmetry")
        legend = rt.TLegend(0.7, 0.65, 0.9, 0.9)
        legend.SetHeader("time resolution = %.3f #mus" % (time_resolution), "C")

        legend.AddEntry(func, 'y = A_{g-2} sin(#omega_{g-2}x + #phi_{g-2})')
        legend.AddEntry(func, 'A_{g-2} = %.3f #pm %.3f' % (func.GetParameter(0), func.GetParError(0)))
        legend.AddEntry(func, '#omega_{g-2} = %.3f #pm %.3f' % (func.GetParameter(1), func.GetParError(1)))
        legend.AddEntry(func, '#phi_{g-2} = %.3f #pm %.3f' % (func.GetParameter(2), func.GetParError(2)))

        precession_freq = func.GetParameter("#omega_{g-2}") * 10 ** 6  # s^-1

        m_mu = 1.883531475e-28
        B=3
        e=1.60217646e-19
        a_theoretical = 11659206 * 10 ** -10  # (g-2)/2  (from PDG)
        a = np.abs(precession_freq * m_mu / B / e)

        canvas1.Draw()

        legend.Draw()
        print(time_resolution)
        if save_asymmetry_graphs:
            canvas1.SaveAs("example_graphs/asymmetry_plot_timeresolution_" + str(run_number) +"_"+str(int(time_resolution*1000))+"ns.png")



        a_err = np.abs(a - a_theoretical) / (a_theoretical) * 100
        print("a      : ", a)
        print("error  : ", a_err, "%")

        a_s.append(a)
        a_errs.append(a_err)

        del canvas1
        del asymmetry_histogram
        del out_in_sum
        del out_in_diff
        del up_histogram
        del down_histogram
        del func
    fig = plt.figure(figsize=(15,9))
    plt.subplot(1, 1, 1)
    plt.grid(which="both")
    plt.loglog(resolutions, np.abs(a_errs))
    plt.xlabel("Time Resolution ($\mu$s)")
    plt.ylabel("$a$ Error (%)")
    plt.savefig("graphs/time_analysis_g-2_" + str(run_number) + "_" + exact_label + ".png")