import ROOT as rt
import root_numpy as rn
import numpy as np
from collections.abc import Iterable
from typing import Any


def add_columns2tree(target_tree: rt.TTree, columns: list[np.array], names: list[str], types=None) -> rt.TTree:
    """
        Adds a column to a tree. The tree is passed by reference.

        NOTE: ROOT.Fill must be called after to add the data to the tree.

        :param target_tree: The tree to which the branch is added
        :param branch_name: The name of the branch to be added
        :param data: The data which is to be added into the branch
        :param dtype: A type string: currently supports
                      {"D": np.float64, "I": np.int32, "C": np.char, "L": np.int64, "l": np.uint64}
        """
    if types is None:
        types = [np.float64 for col in columns]

    for col, name, type in zip(columns, names, types):
        new_col = np.array(col, dtype=[(name, type)])

        target_tree = rn.array2tree(new_col, tree=target_tree)
    return target_tree


def add_branch2tree(target_tree: rt.TTree, branch_name: str, data: Any, dtype: str) -> None:
    """
    Adds a branch to a tree. The tree is passed by reference.

    NOTE: ROOT.Fill must be called after to add the data to the tree.
    Should work most days, but it can segfault when Python is in a mood and deletes your data.

    :param target_tree: The tree to which the branch is added
    :param branch_name: The name of the branch to be added
    :param data: The data which is to be added into the branch
    :param dtype: A type string: currently supports
                  {"D": np.float64, "I": np.int32, "C": np.char, "L": np.int64, "l": np.uint64}
    """
    type_dict = {"D": np.float64, "I": np.int32, "C": np.char, "L": np.int64, "l": np.uint64}
    if isinstance(data, Iterable):
        data = np.array(data, dtype=type_dict[dtype])
        num_entries = data.size
        formatted_cstring = branch_name+"["+str(num_entries)+"]/"+dtype
        branch = target_tree.Branch(branch_name, data, formatted_cstring)
        branch.SetEntries(num_entries)
        branch.Fill()
    else:
        data = type_dict[dtype](data)
        formatted_cstring = branch_name+"/"+dtype
        branch = target_tree.Branch(branch_name, data, formatted_cstring)
        branch.Fill()


def get_list_of_treenames(filename: str) -> [rt.TTree]:
    """
    Returns a list containing all the tree names contained within a root file.

    :param filename: The relative path to the root file
    :return: A list of strings containing the tree names
    """
    input_file = rt.TFile(filename, 'READ')
    keylist = input_file.GetListOfKeys()

    tree_list = []
    for key in keylist:
        tree_name = key.GetName()
        tree_list.append(tree_name)

    return tree_list


if __name__ == '__main__':
    # just some test code
    pixel_sizes = 10 ** (np.linspace(-3, 1, 20))
    pixel_tree_names = ["px=" + str(int(pixel_size * 1000000)) + "nm" for pixel_size in pixel_sizes]
    outputFile = rt.TFile('pixeltrees2.root', 'RECREATE')

    tree_list = []
    for i,pixel_size in enumerate(pixel_sizes):
        tree = rt.TTree(pixel_tree_names[i], "")
        tree_list.append(tree)

        # tree = add_columns2tree(tree, [pixel_size], "pixel_size")
        add_branch2tree(tree, "pixel_size", pixel_size, "D")
        add_branch2tree(tree, "transverse_momentum", [2,3,4,5], "D")
        add_branch2tree(tree, "vertical_momentum", [4,5,6,82,34,1,45], "D")
        add_branch2tree(tree, "transverse_momentum_error", [0.1,0.2,0.12], "D")
        add_branch2tree(tree, "vertical_momentum_error", [0.05, 0.02, 0.1], "D")
        add_branch2tree(tree, "direction", [1, -1, 0], "I")
        tree.Fill()

    outputFile.Write()
    outputFile.Close()


