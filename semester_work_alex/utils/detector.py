import numpy as np

def pixel_rounding(det_x, det_y, det_z, det_ID, pixel_size=0.1):
    """
    Rounds detector coordinates according to a specified pixel size.

    :param det_x: The x coordinates of the detector hits.
    :param det_y: The y coordinates of the detector hits.
    :param det_z: The z coordinates of the detector hits.
    :param det_z: The the detector IDs of the detector hits.
    :param pixel_size: The size of the pixels in mm.
    :return: det_x, det_y, det_z, det_ID arrays, rounded to the pixel size.
    """
    if pixel_size == 0:
        return det_x, det_y, det_z, det_ID

    coords_array = []

    for i, coords in enumerate(zip(det_x, det_y, det_z, det_ID)):
        x, y, z, ID = coords

        # convert to cylindrical coordinates:
        radius = np.sqrt(x ** 2 + y ** 2)
        theta = np.arctan2(y, x)
        dtheta = pixel_size / radius

        theta_pixel = int(theta / dtheta) + 0.5
        rounded_theta = theta_pixel * dtheta

        z_pixel = int(z / pixel_size) + 0.5
        z = z_pixel * pixel_size

        x = radius * np.cos(rounded_theta)
        y = radius * np.sin(rounded_theta)

        coords_array.append((x,y,z,ID))

    # remove duplicate detector hits
    coords_array = list(set(coords_array))
    coords_array.sort(key=lambda a: a[2])
    det_x = np.array([coords[0] for coords in coords_array])
    det_y = np.array([coords[1] for coords in coords_array])
    det_z = np.array([coords[2] for coords in coords_array])
    det_ID = np.array([coords[3] for coords in coords_array])

    return det_x, det_y, det_z, det_ID


def get_detect_info(direction, actual_direction, print_info=False):
    """
    Computes accuracy, efficiency, and other relevant information from the direction array, given a reference direction
    array.

    These numbers represent the effectiveness of the detection method.

    :param direction: The measured direction array. Acceptable values are -1, 0, 1
    :param actual_direction: The actual direction array. Acceptable values are -1, 0, 1
    :param print_info: Flag whether the results should be printed or not. Default=False
    :return: A dictionary containing accuracy, efficiency, false_up, false_down, bias.
    """
    print()
    assert np.array(actual_direction).size == np.array(direction).size, \
        "mismatch in length of direction and actual_direction arrays: " + str(np.array(actual_direction).size) + \
        " and " + str(np.array(direction).size)

    accurately_labelled = (np.array(direction) == np.array(actual_direction))
    if accurately_labelled.size - list(direction).count(0) == 0:
        accuracy = 0
    else:
        accuracy = list(accurately_labelled).count(True) / (accurately_labelled.size - list(direction).count(0)) * 100

    reports_up_while_down = np.logical_and(np.array(direction) == np.ones_like(direction),
                                           np.array(actual_direction) == -np.ones_like(actual_direction))
    reports_down_while_up = np.logical_and(np.array(direction) == -np.ones_like(direction),
                                           np.array(actual_direction) == np.ones_like(actual_direction))

    if (len(direction) - list(direction).count(0)) == 0:
        bias = 0
    else:
        bias = (list(reports_up_while_down).count(True) - list(reports_down_while_up).count(True)) \
           / (len(direction) - list(direction).count(0)) * 100

    false_up = list(reports_up_while_down).count(True)
    false_down = list(reports_down_while_up).count(True)
    efficiency = (1 - list(direction).count(0) / len(direction)) * 100

    if print_info:
        print("false up: ", false_up)
        print("false down: ", false_down)
        print("bias: ", bias, "%")
        print("accuracy: ", accuracy, "%")
        print("efficiency: ", efficiency, " % ")

    result_dict = {"false_up": false_up, "false_down": false_down, "bias": bias, "accuracy": accuracy,
                   "efficiency": efficiency}
    return result_dict