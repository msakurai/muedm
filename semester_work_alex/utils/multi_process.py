from multiprocessing import Process, Queue, Pipe, freeze_support, set_start_method, cpu_count
import time


def _worker_function(function, queues, show_progress = False):
    t0 = time.time()
    while True:
        my_input = queues[0].get()
        if type(my_input) == bool and my_input == False:
            break
        if show_progress:
            print("\033[1A" + str(my_input[0]))
        result = [my_input[0], function(*(my_input[1]))]

        queues[1].put(result)
    if show_progress:
        print("Process completed after %.2f seconds."%(time.time() - t0))
    return


# applies a function to the elements of an array, implementing multiprocessing to do so. (good for computationally expensive operations)
def multi_process(function, args, nonetype=False, iterable_until=1, show_progress=False):
    """
    Applies the function "function" elementwise to an array, utilizing multiprocessing to reduce the processing time.
    Generally multiple arguments can be iterable, as specified by iterable_until.

    :param function: A function which takes the same number of arguments as in the list args.
    :param args: A list of arguments. The arguments up to the list index iterable_until must be iterable, and have the
                 same size. The remaining arguments will be passed as whole objects (not iterated).
    :param nonetype: Specifies whether the function is a nonetype function. i.e. the function returns nothing.
    :param iterable_until: The index up until which the arguments should be passed element-wise.
    :param show_progress: Flag whether to print progress updates to the console.
    :return: A list containing the returned objects from the function for each element of the array.
    """
    num_processes = cpu_count()
    func_args = []
    array_len = 1
    try:
        array_len = args[0].size
    except:
        pass
    try:
        array_len = len(args[0])
    except:
        pass
    for i in range(array_len):
        func_argsi = [arg[i] for arg in args[:iterable_until]] + [arg for arg in args[iterable_until:]]
        func_args.append([i, func_argsi])
    queues = Queue(), Queue()
    if show_progress:
        print("Now processing array index (out of %d):\n0" % (array_len))

    processes = [Process(target=_worker_function, args=(function, queues, show_progress)) for i in range(num_processes)]
    for func_arg in func_args:
        queues[0].put(func_arg)
    for p in processes:
        p.start()

    for i in range(num_processes):
        queues[0].put(False)  # sends stop messages to be received by worker processes

    if not nonetype:
        results = [False for i in range(array_len)]
        for i in range(array_len):
            output = queues[1].get()
            results[output[0]] = output[1]

    for p in processes:
        p.join()

    if nonetype:
        return
    return results