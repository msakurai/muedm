import matplotlib.gridspec

from utils.detector import pixel_rounding
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import scipy.optimize
import numpy as np
import ROOT as rt
import warnings

# silences warnings due to overflow in the exponential function.
warnings.filterwarnings("ignore", category=RuntimeWarning)


class Helix:
    """
    This class uses a helix fitting algorithm to determine the trajectories of positrons, and extracts important
    information, such as the radial / vertical release direction and initial momentum.

    Use the member function set_entry to set the index of the positron to track. The corresponding member variables are
    then updated accordingly. Access the values like so:
    my_helix_object.vertical_direction:     vertical release direction of the positron (-1: down, 0: unknown, 1: up)
    my_helix_object.radial_direction:       radial release direction of the positron (-1: in, 0: unknown, 1: out)
    etc.
    """

    def __init__(self, tree: rt.TTree, variable_radius = True, variable_center = True, variable_phase_velocity = True,
                 pixel_size = 0, entry_index = 0, muon_orbit_radius = 140):
        """
        Creates an instance of the Helix class.

        :param tree: A root tree produced by musrSim.
        :param variable_radius: Flag which specifies whether to use a fixed or variable radius.
        :param variable_center: Flag which specifies whether to use a fixed or variable orbit center.
        :param variable_phase_velocity: Flag which specifies whether to use a fixed or variable phase velocity.
        :param pixel_size: The size of the pixels on the detector.
        :param entry_index: The entry index within the root tree of the positron to be tracked.
        :param muon_orbit_radius: The orbit radius of the muons.
        """
        self.pixel_size = pixel_size

        self.muon_mass = 105.6583755  # MeV
        self.muon_momentum = 125  # MeV
        self.positron_mass = 0.5  # MeV
        self.muon_orbit_radius = muon_orbit_radius  # mm

        self.variable_radius = variable_radius
        self.variable_center = variable_center
        self.variable_phase_velocity = variable_phase_velocity

        self.tree = tree
        self.entry_index = 0
        self.set_entry(entry_index)

    @staticmethod
    def fit_circle_to_3pts(xs, ys):
        """
        Uses the perpendicular bisector method to fit a circle to three points.

        :param xs: Array of x coordinates. Constraint: len == 3
        :param ys: Array of y coordinates. Constraint: len == 3
        :return: r_0: float, x_c: float, y_c: float, success: bool
        """
        assert len(list(xs)) == len(list(ys)) == 3, "Error: fit_circle_to_3pts requires 3 points, but len(xs) = " \
                                                    + str(len(list(xs))) + ", len(ys) = " + str(len(list(ys)))

        # if the algorithm fails for (x,y) due to division by 0, try again for (y,x)
        for xs, ys, swapped in [[xs, ys, False], [ys, xs, True]]:
            if ys[1] - ys[0] == 0 or ys[2] - ys[1] == 0:  # division by 0
                continue

            m_12 = (xs[0] - xs[1]) / (ys[1] - ys[0])
            c_12 = 0.5 * (ys[0] + ys[1] + (xs[1] ** 2 - xs[0] ** 2) / (ys[1] - ys[0]))
            m_23 = (xs[1] - xs[2]) / (ys[2] - ys[1])
            c_23 = 0.5 * (ys[1] + ys[2] + (xs[2] ** 2 - xs[1] ** 2) / (ys[2] - ys[1]))

            if m_12 - m_23 == 0:  # division by 0
                continue

            x_c = (c_23 - c_12) / (m_12 - m_23)
            y_c = m_12 * x_c + c_12

            r_0 = np.sqrt((xs[0] - x_c) ** 2 + (ys[0] - y_c) ** 2)

            if swapped:
                return y_c, x_c, r_0, True
            else:
                return x_c, y_c, r_0, True

        return 0, 0, 0, False

    @staticmethod
    def phase(x, y, x_c, y_c):
        """
        Computes the phase of the coordinates (x,y) relative to the center coordinates (x_c, y_c).

        :param x: x coordinate
        :param y: y coordinate
        :param x_c: x center coordinate
        :param y_c: y center coordinate
        :return: The corresponding phase.
        """
        return np.arctan2(y - y_c, x - x_c)

    def helix_residue(self, params):
        """
        Defines an objective function for the helix. This should be minimized.
        Note that currently this uses Euclidean distance, which is problematic for the phase velocity u.
        The helix can wind arbitrarily tight (u -> infinity) to minimize the vertical distance.

        :param params: Parameters of the fit as given by a = r_0, x_c, y_c, u, phi_0
        :return: A real value which will be minimal for the best fit.
        """
        r_0, x_c, y_c, u, phi_0 = params

        horizontal_diffs = np.array(
            [(r_0 - np.sqrt((x - x_c) ** 2 + (y - y_c) ** 2)) ** 2 for x, y in zip(self.det_x, self.det_y)])
        vertical_diffs = np.array(
            [np.min([(self.det_z - (self.phase(x, y, x_c, y_c) + 2 * np.pi * k - phi_0) / u) ** 2 * u ** 2
                     for k in range(-20, 20)]) for x, y in zip(self.det_x, self.det_y)])

        diffs = horizontal_diffs + vertical_diffs
        diff = np.sum(diffs)
        return diff

    def variable_helix_residue(self, params):
        """
        Defines an objective function for the helix with variable radius, center and phase velocity.
        This should be minimized.

        :param params: Parameters of the fit as given by a = r_0, x_c, y_c, u, phi_0
        :return: A real value which will be minimal for the best fit.
        """
        r_func, x_c_func, y_c_func, phase_fit_func = params

        radial_diffs = np.array([(r_func(z) - np.sqrt((x - x_c_func(z)) ** 2 + (y - y_c_func(z)) ** 2)) ** 2
                                 for x, y, z in zip(self.det_x, self.det_y, self.det_z)])
        phase_diffs = np.array([min(self.phase(x, y, x_c_func(z), y_c_func(z)) - phase_fit_func(z) ** 2,
                                    (2 * np.pi - self.phase(x, y, x_c_func(z), y_c_func(z)) + phase_fit_func(z)) ** 2)
                                for x, y, z in zip(self.det_x, self.det_y, self.det_z)])

        diffs = radial_diffs + phase_diffs
        diff = np.sum(diffs)
        return diff

    def variable_circle_residue(self, params):
        """
        Defines an objective function for circle fitting with variable radius;
        the sum of squares of the Euclidean distance from the detector hits.

        :param params: The functions of z r_func, x_c_func, y_c_func that describe the radius and center coordinate.
        :return: A real value which should be minimized.
        """
        r_func, x_c_func, y_c_func = params

        radial_diffs = np.array([(r_func(z) - np.sqrt((x - x_c_func(z)) ** 2 + (y - y_c_func(z)) ** 2)) ** 2
                                 for x, y, z in zip(self.det_x, self.det_y, self.det_z)])

        diff = np.sum(radial_diffs)
        return diff

    def circle_residue(self, r, x_c, y_c):
        """
        Defines an objective function for circle fitting with fixed radius and center coordinate;
        the sum of squares of the Euclidean distance from the detector hits.

        :param r: The radius of the circle
        :param x_c: The x-coordinate of the circle center
        :param y_c: The y-coordinate of the circle center
        :return: A real value which should be minimized.
        """
        return self.variable_circle_residue([lambda z: r, lambda z: x_c, lambda z: y_c])

    def circle_fitting(self):
        """
        Computes the best circle fit for the detector hits of the current positron (specified by set_entry).

        If the flags variable_radius and/or variable_center are set, the circle fit becomes a function of the vertical
        coordinate.

        The corresponding member variables for the circle fit functions r_func, x_c_func, and y_c_func (for the radius
        and center coordinate respectively) are set.
        """
        # check if the detector data is sufficient
        if not (self.det_z.size >= 3 and len(set(self.det_ID)) >= 2):
            self.message = "Not Found: Insufficient detector hits."
            return False

        det_xyz = np.array([xyz for xyz in zip(self.det_x, self.det_y, self.det_z, self.det_ID)])

        # create slices for circle fits
        xyz_slices = [det_xyz[i:i + 3] for i in range(det_xyz.shape[0] - 2)] \
                      + [[det_xyz[i], det_xyz[i + 2], det_xyz[i + 3]] for i in range(det_xyz.shape[0] - 3)] \
                     + [[det_xyz[i], det_xyz[i + 1], det_xyz[i + 3]] for i in range(det_xyz.shape[0] - 4)] \
                      + [[det_xyz[i], det_xyz[i+2], det_xyz[i+4]] for i in range(det_xyz.shape[0] - 4)]

        xyz_slices = np.array(xyz_slices)
        # axis 0: slice, axis 1: point, axis 2: coordinate

        # parameters for circles for all triples of consecutive points
        x_cs = []
        y_cs = []
        rs = []
        circle_zs = []

        # compute circles for each 3 consecutive points
        for i in range(len(xyz_slices)):
            x_c, y_c, r, success = self.fit_circle_to_3pts(xyz_slices[i, :, 0], xyz_slices[i, :, 1])

            if success:
                x_cs.append(x_c)
                y_cs.append(y_c)
                rs.append(r)
                circle_zs.append(np.mean(xyz_slices[i, :, 2]))  # add the mean z coordinate of the circle

        residue = float('inf')

        if len(rs) < 1:
            self.message = "Not Found: Error while computing circle fits due to division by 0."
            return False
        # determine the circle with the sum of the square distance to the detector hits
        for x_c, y_c, r in zip(x_cs, y_cs, rs):
            diff = np.sum([(r - np.sqrt((x - x_c) ** 2 + (y - y_c) ** 2)) ** 2 for x, y in zip(self.det_x, self.det_y)])
            if diff < residue:
                residue = diff
                optimal_params = x_c, y_c, r

        # Compute r, x_c and y_c as linear functions of z
        x_c, y_c, r = optimal_params

        objective_function = lambda params: np.sum((params[2] - np.sqrt((self.det_x - params[0]) ** 2 + (self.det_y - params[1]) ** 2)) ** 2)
        try:
            optimized_result = scipy.optimize.minimize(objective_function, list(optimal_params))
            if optimized_result.success:
                optimal_params = optimized_result.x
                x_c, y_c, r = optimal_params
        except:
            pass

        if len(circle_zs) > 6: #and max(circle_zs) - min(circle_zs) > 0:
            if self.variable_radius:

                try:
                    r_fit_func = lambda x, r_0, b: r_0 * np.exp(-b * x)
                    r_params = curve_fit(r_fit_func, circle_zs, rs, p0=[r, 0])[0]
                except:
                    r_params = [r, 0]
                # r_params = np.polyfit(circle_zs, rs, deg=1) # linear
            else:
                # r_params = np.array([0, r]) # linear
                r_params = np.array([r, 0])

            if self.variable_center:
                x_c_params = np.polyfit(circle_zs, x_cs, deg=1)
                y_c_params = np.polyfit(circle_zs, y_cs, deg=1)
            else:
                x_c_params = np.array([0, x_c])
                y_c_params = np.array([0, y_c])

        else:
            # r_params = np.array([0, r])  linear
            r_params = np.array([r, 0])
            x_c_params = np.array([0, x_c])
            y_c_params = np.array([0, y_c])

        # r_func = lambda z: r_params[0] * z + r_params[1]
        r_func = lambda z: r_params[0] * np.exp(-r_params[1] * z)
        x_c_func = lambda z: x_c_params[0] * z + x_c_params[1]
        y_c_func = lambda z: y_c_params[0] * z + y_c_params[1]

        variable_residue = self.variable_circle_residue([r_func, x_c_func, y_c_func])
        fixed_residue = self.circle_residue(r, x_c, y_c)

        if fixed_residue < variable_residue:
            # print("variable fit chosen")
            r_params = np.array([r, 0])
            x_c_params = np.array([0, x_c])
            y_c_params = np.array([0, y_c])
            r_func = lambda z: r_params[0] * np.exp(-r_params[1] * z)
            x_c_func = lambda z: x_c_params[0] * z + x_c_params[1]
            y_c_func = lambda z: y_c_params[0] * z + y_c_params[1]
        else:
            pass
            # print("fixed fit chosen")

        self.r_func = r_func
        self.x_c_func = x_c_func
        self.y_c_func = y_c_func
        self.r_params = r_params
        self.x_c_params = x_c_params
        self.y_c_params = y_c_params
        self.circle_zs = circle_zs
        return True

    @staticmethod
    def compute_momentum(r_func, phase_func):
        """
        Computes the transverse and vertical momentum from the fit functions.
        :param r_func: The radius fit function
        :param phase_func: The phase fit function
        :return:
        """
        transverse_momentum = 1 / 3.33564 * r_func(0) / 1000 * 1 * 3 * 1000  # MeV
        dz = 1e-1  # numerical derivative interval
        phase_velocity = (phase_func(dz) - phase_func(0)) / dz

        vertical_momentum = 1 / phase_velocity / r_func(0) * transverse_momentum

        # determine the direction
        if vertical_momentum > 0:
            direction = 1
        else:
            direction = -1

        return transverse_momentum, vertical_momentum, direction

    def phase_fitting(self):
        """
        Determines the direction of the positron by monotonizing the phase (with respect to the center coordinates of
        the circle fit) in both directions, and choosing the one which best fits a linear fit.

        If the flag variable_phase_velocity is set, this linear fit is done over smaller intervals (but still larger
        than one rotation), and the minimum least square distance is chosen. This allows the phase velocity to change,
        but periodic non-linearities (due to monotonizing in the wrong direction) are still detected.


        Note that for this algorithm to work effectively, it is best to have detector hits at three different phases.
        If there are only two detector hits per rotation with a phase offset of pi, the direction cannot be determined.
        :return:
        """
        phase_func_of_params = lambda z, params: params[0] * z + params[1]

        # determine the phase of the points with respect to the optimal circle
        det_phi = np.arctan2(self.det_y - self.y_c_func(self.det_z), self.det_x - self.x_c_func(self.det_z))

        # assume the phase velocity is negative/positive
        self.det_phi_positive_slope = np.copy(det_phi)
        self.det_phi_negative_slope = np.copy(det_phi)

        # force the phase velocity positive or negative by adding/subtracting 2pi
        for i in range(1, det_phi.size):
            if self.det_phi_positive_slope[i] - self.det_phi_positive_slope[i - 1] < -0.1:
                self.det_phi_positive_slope[i:] = self.det_phi_positive_slope[i:] + 2 * np.pi

        for i in range(1, det_phi.size):
            if self.det_phi_negative_slope[i] - self.det_phi_negative_slope[i - 1] > 0.1:
                self.det_phi_negative_slope[i:] = self.det_phi_negative_slope[i:] - 2 * np.pi

        # compute a linear fit for the phase (assumes constant phase velocity)
        if len(set(self.det_z)) < 3:
            self.message = "Not Found: Not enough points for linear fit."
            self.success = False
            return False

        if len(set(self.det_z)) <= 10 or not self.variable_phase_velocity:
            positive_fit = np.polyfit(self.det_z, self.det_phi_positive_slope, deg=1)
            negative_fit = np.polyfit(self.det_z, self.det_phi_negative_slope, deg=1)

            self.positive_fit_evaluated = positive_fit[0] * self.det_z + positive_fit[1]
            self.negative_fit_evaluated = negative_fit[0] * self.det_z + negative_fit[1]

            # compute the sum of absolutes difference to the phases
            positive_diff = np.sum(np.abs(self.positive_fit_evaluated - self.det_phi_positive_slope))
            negative_diff = np.sum(np.abs(self.negative_fit_evaluated - self.det_phi_negative_slope))

            self.phase_fit = positive_fit
            if negative_diff < positive_diff:
                self.phase_fit = negative_fit

        else:
            det_z_windows = [self.det_z[i:i + 10] for i in range(len(self.det_z) - 10)]
            det_phi_positive_windows = [self.det_phi_positive_slope[i:i + 10] for i in
                                        range(len(self.det_phi_positive_slope) - 10)]
            det_phi_negative_windows = [self.det_phi_negative_slope[i:i + 10] for i in
                                        range(len(self.det_phi_negative_slope) - 10)]

            positive_fits = [np.polyfit(zs, phis, deg=1) for zs, phis in zip(det_z_windows, det_phi_positive_windows)]
            negative_fits = [np.polyfit(zs, phis, deg=1) for zs, phis in zip(det_z_windows, det_phi_negative_windows)]

            positive_fits_evaluated = [fit[0] * det_z_window + fit[1]
                                       for fit, det_z_window in zip(positive_fits, det_z_windows)]
            negative_fits_evaluated = [fit[0] * det_z_window + fit[1]
                                       for fit, det_z_window in zip(negative_fits, det_z_windows)]

            positive_diffs = [np.sum(np.abs(fit_evaluated - det_phi_window))
                              for fit_evaluated, det_phi_window in
                              zip(positive_fits_evaluated, det_phi_positive_windows)]
            negative_diffs = [np.sum(np.abs(fit_evaluated - det_phi_window))
                              for fit_evaluated, det_phi_window in
                              zip(negative_fits_evaluated, det_phi_negative_windows)]

            if np.min(negative_diffs) < np.min(positive_diffs):
                fits = negative_fits
                diffs = negative_diffs
            else:
                fits = positive_fits
                diffs = positive_diffs

            # check for impossible momentum results (violation of energy conservation)
            # if only one fit does not violate momentum conservation, choose that one.
            transverse_momentum1, vertical_momentum1, _ = self.compute_momentum(self.r_func,
                                                                           lambda z: phase_func_of_params(z, fits[0]))
            unrealistic1 = np.sqrt(vertical_momentum1 ** 2 + transverse_momentum1 ** 2) > self.muon_mass \
                           + self.muon_momentum - self.positron_mass
            transverse_momentum2, vertical_momentum2, _ = self.compute_momentum(self.r_func,
                                                                           lambda z: phase_func_of_params(z, fits[-1]))
            unrealistic2 = np.sqrt(vertical_momentum2 ** 2 + transverse_momentum2 ** 2) > self.muon_mass \
                           + self.muon_momentum - self.positron_mass

            if unrealistic1 and unrealistic2:
                self.message = "Error: An impossible momentum result was found."
                return False
            elif unrealistic1 and not unrealistic2:
                self.phase_fit = fits[-1]
            elif unrealistic2 and not unrealistic1:
                self.phase_fit = fits[0]
            else:
                if diffs[-1] < diffs[0]:
                    self.phase_fit = fits[-1]
                else:
                    self.phase_fit = fits[0]

            self.positive_fit_evaluated = positive_fits_evaluated[0]
            self.negative_fit_evaluated = negative_fits_evaluated[0]

        self.phase_func = lambda z: phase_func_of_params(z, self.phase_fit)
        self.success = True
        return True

    def set_unknown(self):
        """
        Sets unknown parameters to nan, meant to be used in case the algorithm fails.
        """
        self.success = False
        self.circle_success = False
        self.initial_momentum_x = float("nan")
        self.initial_momentum_y = float("nan")
        self.initial_momentum_z = float("nan")
        self.initial_position_x = float("nan")
        self.initial_position_y = float("nan")
        self.initial_position_z = float("nan")
        self.transverse_momentum = float("nan")
        self.vertical_momentum = float("nan")
        self.time = float("nan")

        self.vertical_direction = 0
        self.radial_direction = 0

    def set_entry(self, entry_index: int):
        """
        Sets the index of the positron being tracked, and recalculates the member variables accordingly.

        :param entry_index: The entry for which the positron is tracked
        """
        # if self.entry_index == entry_index:  # No need to recalculate
        #     return

        self.entry_index = entry_index

        # Get relevant detector information
        self.tree.GetEntry(entry_index)
        self.det_x = np.array(self.tree.det_x)
        self.det_y = np.array(self.tree.det_y)
        self.det_z = np.array(self.tree.det_z)
        self.det_ID = np.array(self.tree.det_ID)

        self.det_time_start = np.array(self.tree.det_time_start)

        # check if there are sufficient detector hits
        if self.det_ID.size != 0:
            self.detected = 1
        else:
            self.detected = 0
            self.message = "Failed: No detector hits."
            self.set_unknown()
            return

        # DETECTOR ROUNDING
        self.det_x, self.det_y, self.det_z, self.det_ID = pixel_rounding(self.det_x, self.det_y, self.det_z,
                                                                         self.det_ID, self.pixel_size)

        # remove consecutive detector hits that are too close
        consecutive_distance = np.sqrt(
            (self.det_x[1:] - self.det_x[:-1]) ** 2 + (self.det_y[1:] - self.det_y[:-1]) ** 2 + (
                    self.det_z[1:] - self.det_z[:-1]) ** 2)
        remove = np.array([False] + list(consecutive_distance < np.ones_like(consecutive_distance) * 2))
        self.det_x = self.det_x[np.logical_not(remove)]
        self.det_y = self.det_y[np.logical_not(remove)]
        self.det_z = self.det_z[np.logical_not(remove)]
        self.det_ID = self.det_ID[np.logical_not(remove)]

        self.max_z = np.max(self.det_z)
        self.min_z = np.min(self.det_z)

        # sort according to z coordinate
        sorted_idx = np.argsort(self.det_z)
        self.det_x = self.det_x[sorted_idx]
        self.det_y = self.det_y[sorted_idx]
        self.det_z = self.det_z[sorted_idx]
        self.det_ID = self.det_ID[sorted_idx]

        if self.det_time_start.size > 0:
            det_time = self.det_time_start[0]
        else:
            det_time = 0

        # CIRCLE FITTING
        success = self.circle_fitting()
        self.circle_success = success
        if not success:
            self.set_unknown()
            return

        # DETERMINING VERTICAL MOMENTUM
        success = self.phase_fitting()

        if not success:
            self.message = "Failed: Phase Fitting Failed."

            self.set_unknown()
            return

        # compute transverse and vertical momentum.
        transverse_momentum, vertical_momentum, vertical_direction = self.compute_momentum(self.r_func,
                                                                                           self.phase_func)

        # compute whether the positron was moving inwards or outwards with respect to orbit

        # compute intersection of helix with muon orbit
        func = lambda z: (self.x_c_func(z) + self.r_func(z) * np.cos(self.phase_func(z))) ** 2 \
                         + (self.y_c_func(z) + self.r_func(z) * np.sin(self.phase_func(z))) ** 2 \
                         - self.muon_orbit_radius ** 2
        converged = False
        offset = 1
        z = None
        while not converged:
            z_plus, results_pos = scipy.optimize.newton(func, offset, full_output=True, disp=False)
            z_min, results_neg = scipy.optimize.newton(func, offset, full_output=True, disp=False)

            zs = np.array([z_plus, z_min]).reshape(-1)[np.array([results_pos.converged, results_neg.converged])]
            z = 0
            if zs.size != 0 and np.min(np.abs(zs)) < 20:
                z = zs[np.argmin(np.abs(zs))]
                converged = True

            if offset > 10:
                converged = False
                break
            offset += 1

        if converged:
            theta = np.arctan2(self.y_c_func(z), self.x_c_func(z))
            x = self.x_c_func(z) + self.r_func(z) * np.cos(self.phase_func(z))
            y = self.y_c_func(z) + self.r_func(z) * np.sin(self.phase_func(z))
            alpha = self.phase_func(z)
            alpha = np.arctan2(y, x)

            if np.sin(alpha - theta) < 0:  # -np.pi/2 + alpha <= theta < np.pi/2 + alpha
                radial_direction = 1
            elif np.sin(alpha - theta) > 0:
                radial_direction = -1
            else:
                radial_direction = 0

            if np.abs(2 * np.pi / self.phase_fit[0]) < 30:
                radial_direction = 0

            # determine the momentum at the decay vertex
            helix_func = lambda z1: np.array([self.x_c_func(z1) + self.r_func(z1) * np.cos(self.phase_func(z1)),
                                              self.y_c_func(z1) + self.r_func(z1) * np.sin(self.phase_func(z1))])
            dz = 2**-10
            direction = (helix_func(z+dz) - helix_func(z)) / dz
            direction = direction / np.sqrt(np.sum(direction**2)) * np.sign(self.phase_fit[0])

            self.initial_momentum_x = direction[0] * transverse_momentum
            self.initial_momentum_y = direction[1] * transverse_momentum
            self.initial_momentum_z = transverse_momentum / self.r_func(z) / self.phase_fit[0]

            self.initial_position_x = x
            self.initial_position_y = y
            self.initial_position_z = z

            initial_position = np.array([x, y, z])
            initial_momentum = np.array([self.initial_momentum_x, self.initial_momentum_y, self.initial_momentum_z])

            radial_direction = np.sign(np.dot(initial_position, initial_momentum))
            if np.isnan(radial_direction):
                radial_direction = 0
                self.message = "Failed: Radial direction could not be determined."
                self.set_unknown()
        else:
            radial_direction = 0
            self.message = "Failed: Radial direction could not be determined."
            self.set_unknown()

        # check if this can be a physical trajectory
        unrealistic = np.sqrt(vertical_momentum ** 2 + transverse_momentum ** 2) > self.muon_mass + self.muon_momentum \
                      - self.positron_mass

        if np.abs(np.arctan2(vertical_momentum, transverse_momentum)) < 0.001:
            self.vertical_direction = 0
        else:
            self.vertical_direction = vertical_direction

        self.radial_direction = radial_direction
        self.vertical_direction = vertical_direction
        self.success = True
        self.message = "Success!"
        self.transverse_momentum = transverse_momentum
        self.vertical_momentum = vertical_momentum
        self.time = det_time

    def plot(self, fig):
        """
        Plots three graphs relating to the current trajectory (which is specified by set_entry).
        The graphs are:
            - A graph in the x-y plane showing the circle fit.
            - A graph of the phase as a function of the vertical coordinate z, showing the detector hits and best fit.
            - A 3D plot of the trajectory.

        If a circle fit wasn't not found, the positron cannot be tracked and hence the trajectory is not found.
        In this case, the function will not plot and return False.

        :returns: A boolean flag which states whether the trajectory was plotted or not.
        """
        if not self.circle_success or not self.success:
            return False

        gs = matplotlib.gridspec.GridSpec(2,2,figure=fig)
        fig.add_subplot(gs[0,0])
        plt.xlabel("$x$ position (mm)")
        plt.ylabel("$y$ position (mm)")

        thetas = np.linspace(0, 2 * np.pi, 1000)
        for circle_z in self.circle_zs:
            plt.plot(self.x_c_func(circle_z) + self.r_func(circle_z) * np.cos(thetas),
                     self.y_c_func(circle_z) + self.r_func(circle_z) * np.sin(thetas), color="orange")

        plt.xlim(-150, 150)
        plt.ylim(-150, 150)

        plt.plot(70 * np.cos(thetas), 70 * np.sin(thetas), color="grey", linestyle="--")
        plt.plot(110 * np.cos(thetas), 110 * np.sin(thetas), color="grey", linestyle="--")
        plt.plot(120 * np.cos(thetas), 120 * np.sin(thetas), color="grey", linestyle="--")

        for x, y, z, t in zip(self.det_x, self.det_y, self.det_z, self.det_time_start):
            plt.scatter(x, y, color="red", s=20)

        fig.add_subplot(gs[1,:])
        plt.xlabel("$z$ position (mm)")
        plt.ylabel("phase (rad)")

        plt.scatter(self.det_z, self.det_phi_positive_slope, color="red")
        plt.scatter(self.det_z, self.det_phi_negative_slope, color="blue")

        plt.plot(self.det_z, self.phase_func(self.det_z), color="red", linestyle="--")
        plt.ylim(min(self.det_phi_negative_slope), max(self.det_phi_positive_slope))
        # plt.plot(det_z[:6], negative_fit_evaluated, color="blue", linestyle="--")

        if not self.variable_phase_velocity:
            # find the optimal phi (the phi_0 from the linear fit is too imprecise)
            # this does not affect transverse/vertical momentum, and is therefore only done for the sake of plotting
            u = self.phase_fit[0]
            phis = np.linspace(0, 2 * np.pi, 100)
            phi_residues = [self.helix_residue([self.r_func(self.det_z), self.x_c_func(self.det_z),
                                                self.y_c_func(self.det_z), u, phi_1]) for phi_1 in phis]
            phi_0 = phis[np.argmin(phi_residues)]
            fit = (self.phase_fit[0], phi_0)

        ax = fig.add_subplot(gs[0,1], projection='3d')
        ax.set_xlabel("$x$ position (mm)")
        ax.set_ylabel("$y$ position (mm)")
        ax.set_zlabel("$z$ position (mm)")


        if self.vertical_direction is None:
            zs1 = np.linspace(-300, 300, 1000)
        elif self.vertical_direction == 1:
            zs1 = np.linspace(0, 300, 1000)
        else:
            zs1 = np.linspace(-300, 0, 1000)

        ax.plot(self.x_c_func(zs1) + self.r_func(zs1) * np.cos(self.phase_func(zs1)),
                self.y_c_func(zs1) + self.r_func(zs1) * np.sin(self.phase_func(zs1)), zs1)
        ax.scatter(self.det_x, self.det_y, self.det_z, color="red")

        for z in np.linspace(-300, 300, 3):
            plt.plot(120 * np.cos(thetas), 120 * np.sin(thetas), np.ones_like(thetas) * z, linestyle="--",
                     color="grey")
            plt.plot(70 * np.cos(thetas), 70 * np.sin(thetas), np.ones_like(thetas) * z, linestyle="--",
                     color="grey")
            plt.plot(110 * np.cos(thetas), 110 * np.sin(thetas), np.ones_like(thetas) * z, linestyle="--",
                     color="grey")

        return True