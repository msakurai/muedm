import numpy as np
import matplotlib.pyplot as plt
import ROOT as rt
import scipy.optimize

from utils.detector import pixel_rounding
from scipy.optimize import curve_fit
failcount=0

"""This is the old non-object oriented version of Helix.py. (DEPRECATED)"""

def fit_circle_to_3pts(xs, ys):
    """
    Fits a circle to three points.

    :param xs: Array of x coordinates. Constraint: len == 3
    :param ys: Array of y coordinates. Constraint: len == 3
    :return: r_0: float, x_c: float, y_c: float, success: bool
    """
    assert len(list(xs)) == len(list(ys)) == 3, "Error: fit_circle_to_3pts requires 3 points, but len(xs) = "\
                                                + str(len(list(xs))) + ", len(ys) = " + str(len(list(ys)))

    if ys[1] - ys[0] == 0 or ys[2] - ys[1] == 0:
        return 0, 0, 0, False

    m_12 = (xs[0] - xs[1]) / (ys[1] - ys[0])
    c_12 = 0.5 * (ys[0] + ys[1] + (xs[1] ** 2 - xs[0] ** 2) / (ys[1] - ys[0]))
    m_23 = (xs[1] - xs[2]) / (ys[2] - ys[1])
    c_23 = 0.5 * (ys[1] + ys[2] + (xs[2] ** 2 - xs[1] ** 2) / (ys[2] - ys[1]))

    if m_12 - m_23 == 0:
        return 0, 0, 0, False
    x_c = (c_23 - c_12) / (m_12 - m_23)
    y_c = m_12 * x_c + c_12

    r_0 = np.sqrt((xs[0] - x_c) ** 2 + (ys[0] - y_c) ** 2)

    return x_c, y_c, r_0, True


def phase(x, y, x_c, y_c):
    """
    Computes the phase of the coordinates (x,y) relative to the center coordinates (x_c, y_c).
    
    :param x: x coordinate
    :param y: y coordinate
    :param x_c: x center coordinate
    :param y_c: y center coordinate
    :return: The corresponding phase.
    """
    return np.arctan2(y - y_c, x - x_c)


def helix_residue(a, det_x, det_y, det_z):
    """
    Defines an objective function for the helix. This should be minimized.
    
    TODO: Currently the objective function does not support changing u. Using Euclidian distance as the objective
     function, this causes the helix to wind as tight as possible to minimize distance (u -> \infty).
     
    :param a: Parameters of the fit as given by a = r_0, x_c, y_c, u, phi_0
    :return: A real value which will be minimal for the best fit.
    """
    r_0, x_c, y_c, u, phi_0 = a

    horizontal_diffs = np.array([(r_0 - np.sqrt((x - x_c) ** 2 + (y - y_c) ** 2)) ** 2 for x, y in zip(det_x, det_y)])
    vertical_diffs = np.array([np.min([(det_z - (phase(x, y, x_c, y_c) + 2 * np.pi * k - phi_0) / u) ** 2 * u ** 2
                                       for k in range(-20, 20)]) for x, y in zip(det_x, det_y)])

    diffs = horizontal_diffs + vertical_diffs
    diff = np.sum(diffs)
    return diff


def variable_helix_residue(a, det_x, det_y, det_z):

    r_func, x_c_func, y_c_func, phase_fit_func = a

    radial_diffs = np.array([(r_func(z) - np.sqrt((x - x_c_func(z)) ** 2 + (y - y_c_func(z)) ** 2)) ** 2
                                 for x, y, z in zip(det_x, det_y, det_z)])
    phase_diffs = np.array([min(phase(x,y,x_c_func(z),y_c_func(z)) - phase_fit_func(z)**2,
                            (2*np.pi - phase(x, y, x_c_func(z), y_c_func(z)) + phase_fit_func(z)) ** 2)
                                 for x, y, z in zip(det_x, det_y, det_z)])

    diffs = radial_diffs + phase_diffs
    diff = np.sum(diffs)
    return diff


def variable_circle_residue(a, det_x, det_y, det_z):
    r_func, x_c_func, y_c_func = a

    radial_diffs = np.array([(r_func(z) - np.sqrt((x - x_c_func(z)) ** 2 + (y - y_c_func(z)) ** 2)) ** 2
                                 for x, y, z in zip(det_x, det_y, det_z)])

    diff = np.sum(radial_diffs)
    return diff


def circle_residue(det_x, det_y, r, x_c, y_c):
    return variable_circle_residue([lambda z: r, lambda z: x_c, lambda z: y_c], det_x, det_y, np.zeros_like(det_x))


def plot_helix(det_x, det_y, det_z, r_func, x_c_func, y_c_func, tree, circle_zs, det_phi_positive_slope,
               det_phi_negative_slope, variable_phase_velocity, positive_fit_evaluated, negative_fit_evaluated, fit,
               fit_function, direction = None):
    fig = plt.figure(figsize=(15, 5))
    fig.add_subplot(1, 3, 1)
    thetas = np.linspace(0, 2 * np.pi, 1000)

    for circle_z in circle_zs:
        plt.plot(x_c_func(circle_z) + r_func(circle_z) * np.cos(thetas),
                 y_c_func(circle_z) + r_func(circle_z) * np.sin(thetas), color="orange")

    plt.xlim(-150, 150)
    plt.ylim(-150, 150)

    plt.plot(70 * np.cos(thetas), 70 * np.sin(thetas), color="grey", linestyle="--")
    plt.plot(110 * np.cos(thetas), 110 * np.sin(thetas), color="grey", linestyle="--")
    plt.plot(120 * np.cos(thetas), 120 * np.sin(thetas), color="grey", linestyle="--")

    for x, y, z, t in zip(det_x, det_y, det_z, tree.det_time_start):
        plt.scatter(x, y, color="red", s=20)

    fig.add_subplot(1, 3, 2)

    plt.scatter(det_z, det_phi_positive_slope, color="red")
    plt.scatter(det_z, det_phi_negative_slope, color="blue")

    plt.plot(det_z, fit_function(det_z), color="red", linestyle="--")
    plt.ylim(min(det_phi_negative_slope), max(det_phi_positive_slope))
    # plt.plot(det_z[:6], negative_fit_evaluated, color="blue", linestyle="--")

    if not variable_phase_velocity:
        # find the optimal phi (the phi_0 from the linear fit is too imprecise)
        # this does not affect transverse/vertical momentum, and is therefore only done for the sake of plotting
        u = fit[0]
        phis = np.linspace(0, 2 * np.pi, 100)
        phi_residues = [helix_residue([r_func(det_z), x_c_func(det_z), y_c_func(det_z), u, phi_1],
                                      det_x, det_y, det_z) for phi_1 in phis]
        phi_0 = phis[np.argmin(phi_residues)]
        fit = (fit[0], phi_0)

    ax = fig.add_subplot(1, 3, 3, projection='3d')
    if direction is None:
        zs1 = np.linspace(-300, 300, 1000)
    elif direction == 1:
        zs1 = np.linspace(0, 300, 1000)
    else:
        zs1 = np.linspace(-300, 0, 1000)

    ax.plot(x_c_func(zs1) + r_func(zs1) * np.cos(fit_function(zs1)),
            y_c_func(zs1) + r_func(zs1) * np.sin(fit_function(zs1)), zs1)
    ax.scatter(det_x, det_y, det_z, color="red")

    for z in np.linspace(-300, 300, 3):
        plt.plot(120 * np.cos(thetas), 120 * np.sin(thetas), np.ones_like(thetas) * z, linestyle="--",
                 color="grey")
        plt.plot(70 * np.cos(thetas), 70 * np.sin(thetas), np.ones_like(thetas) * z, linestyle="--",
                 color="grey")
        plt.plot(110 * np.cos(thetas), 110 * np.sin(thetas), np.ones_like(thetas) * z, linestyle="--",
                 color="grey")

    return fig

def circle_fitting(det_x, det_y, det_z, det_ID, variable_radius = True, variable_center = True):
    # check if the detector data is sufficient
    if not (det_z.size >= 3 and len(set(det_ID)) >= 2):
        return False, "Not Found: Insufficient detector hits."

    det_xyz = np.array([xyz for xyz in zip(det_x, det_y, det_z, det_ID)])

    # create slices for circle fits
    xyz_slices = [det_xyz[i:i + 3] for i in range(det_xyz.shape[0] - 2)] \
                  + [[det_xyz[i], det_xyz[i + 2], det_xyz[i + 3]] for i in range(det_xyz.shape[0] - 3)] \
                 + [[det_xyz[i], det_xyz[i + 1], det_xyz[i + 3]] for i in range(det_xyz.shape[0] - 4)] \
                  + [[det_xyz[i], det_xyz[i+2], det_xyz[i+4]] for i in range(det_xyz.shape[0] - 4)]
    xyz_slices = [xyz_slice for xyz_slice in xyz_slices if len(set(np.array(xyz_slice)[:, 3])) > 1]
    xyz_slices = np.array(xyz_slices)
    # axis 0: slice, axis 1: point, axis 2: coordinate

    # parameters for circles for all triples of consecutive points
    x_cs = []
    y_cs = []
    rs = []
    circle_zs = []

    # compute circles for each 3 consecutive points
    for i in range(len(xyz_slices)):
        x_c, y_c, r, success = fit_circle_to_3pts(xyz_slices[i, :, 0], xyz_slices[i, :, 1])
        if success:
            x_cs.append(x_c)
            y_cs.append(y_c)
            rs.append(r)
            circle_zs.append(np.mean(xyz_slices[i, :, 2]))  # add the mean z coordinate of the circle

    residue = float('inf')

    if len(rs) < 1:
            return False, "Not Found: Error while computing circle fits."
    # determine the circle with the sum of the square distance to the detector hits
    for x_c, y_c, r in zip(x_cs, y_cs, rs):
        diff = np.sum([(r - np.sqrt((x - x_c) ** 2 + (y - y_c) ** 2)) ** 2 for x, y in zip(det_x, det_y)])
        if diff < residue:
            residue = diff
            optimal_params = x_c, y_c, r

    # Compute r, x_c and y_c as linear functions of z
    x_c, y_c, r = optimal_params

    objective_function = lambda params: np.sum((params[2] - np.sqrt((det_x-params[0])**2 + (det_y-params[1])**2))**2)
    try:
        optimized_result = scipy.optimize.minimize(objective_function, list(optimal_params))
        if optimized_result.success:
            optimal_params = optimized_result.x
            x_c, y_c, r = optimal_params
    except:
        pass


    if len(circle_zs) > 6 and max(circle_zs) - min(circle_zs) > 0:
        if variable_radius:
            try:
                r_fit_func = lambda x, r_0, b: r_0 * np.exp(-b * x)
                r_params = curve_fit(r_fit_func, circle_zs, rs, p0 = [r, 0])[0]
            except:
                r_params = [r, 0]
            # r_params = np.polyfit(circle_zs, rs, deg=1) # linear
        else:
            # r_params = np.array([0, r]) # linear
            r_params = np.array([r, 0])

        if variable_center:
            x_c_params = np.polyfit(circle_zs, x_cs, deg=1)
            y_c_params = np.polyfit(circle_zs, y_cs, deg=1)
        else:
            x_c_params = np.array([0, x_c])
            y_c_params = np.array([0, y_c])

    else:
        # r_params = np.array([0, r])  linear
        r_params = np.array([r, 0])
        x_c_params = np.array([0, x_c])
        y_c_params = np.array([0, y_c])

    # r_func = lambda z: r_params[0] * z + r_params[1]
    r_func = lambda z: r_params[0] * np.exp(-r_params[1] * z)
    x_c_func = lambda z: x_c_params[0] * z + x_c_params[1]
    y_c_func = lambda z: y_c_params[0] * z + y_c_params[1]

    variable_residue = variable_circle_residue([r_func, x_c_func, y_c_func],det_x, det_y, det_z)
    fixed_residue = circle_residue(det_x, det_y, r, x_c, y_c)

    if fixed_residue < variable_residue:
        r_params = np.array([r, 0])
        x_c_params = np.array([0, x_c])
        y_c_params = np.array([0, y_c])
        r_func = lambda z: r_params[0] * np.exp(-r_params[1] * z)
        x_c_func = lambda z: x_c_params[0] * z + x_c_params[1]
        y_c_func = lambda z: y_c_params[0] * z + y_c_params[1]


    return True, [r_func, x_c_func, y_c_func, r_params, x_c_params, y_c_params, circle_zs]


def phase_fitting(det_x, det_y, det_z, r_func, x_c_func, y_c_func, variable_phase_velocity = True):
    fit_function_params = lambda z, params: params[0] * z + params[1]

    # determine the phase of the points with respect to the optimal circle
    det_phi = np.arctan2(det_y - y_c_func(det_z), det_x - x_c_func(det_z))

    # assume the phase velocity is negative/positive
    det_phi_positive_slope = np.copy(det_phi)
    det_phi_negative_slope = np.copy(det_phi)

    # force the phase velocity positive or negative by adding/subtracting 2pi
    for i in range(1, det_phi.size):
        if det_phi_positive_slope[i] - det_phi_positive_slope[i - 1] < -0.1:
            det_phi_positive_slope[i:] = det_phi_positive_slope[i:] + 2 * np.pi

    for i in range(1, det_phi.size):
        if det_phi_negative_slope[i] - det_phi_negative_slope[i - 1] > 0.1:
            det_phi_negative_slope[i:] = det_phi_negative_slope[i:] - 2 * np.pi

    # compute a linear fit for the phase (assumes constant phase velocity)
    if len(set(det_z)) < 3:
        return False, "Not Found: Not enough points for linear fit."
    if  len(set(det_z)) <= 10 or not variable_phase_velocity:
        positive_fit = np.polyfit(det_z, det_phi_positive_slope, deg=1)
        negative_fit = np.polyfit(det_z, det_phi_negative_slope, deg=1)

        positive_fit_evaluated = positive_fit[0] * det_z + positive_fit[1]
        negative_fit_evaluated = negative_fit[0] * det_z + negative_fit[1]

        # compute the sum of absolutes difference to the phases
        positive_diff = np.sum(np.abs(positive_fit_evaluated - det_phi_positive_slope))
        negative_diff = np.sum(np.abs(negative_fit_evaluated - det_phi_negative_slope))

        fit = positive_fit
        if negative_diff < positive_diff:
            fit = negative_fit

    else:
        det_z_windows = [det_z[i:i+10] for i in range(len(det_z) - 10)]
        det_phi_positive_windows = [det_phi_positive_slope[i:i + 10] for i in range(len(det_phi_positive_slope) - 10)]
        det_phi_negative_windows = [det_phi_negative_slope[i:i + 10] for i in range(len(det_phi_negative_slope) - 10)]

        positive_fits = [np.polyfit(zs, phis, deg=1) for zs, phis in zip(det_z_windows, det_phi_positive_windows)]
        negative_fits = [np.polyfit(zs, phis, deg=1) for zs, phis in zip(det_z_windows, det_phi_negative_windows)]

        positive_fits_evaluated = [fit[0] * det_z_window + fit[1]
                                   for fit, det_z_window in zip(positive_fits, det_z_windows)]
        negative_fits_evaluated = [fit[0] * det_z_window + fit[1]
                                   for fit, det_z_window in zip(negative_fits, det_z_windows)]

        positive_diffs = [np.sum(np.abs(fit_evaluated - det_phi_window))
                          for fit_evaluated, det_phi_window in zip(positive_fits_evaluated, det_phi_positive_windows)]
        negative_diffs = [np.sum(np.abs(fit_evaluated - det_phi_window))
                          for fit_evaluated, det_phi_window in zip(negative_fits_evaluated, det_phi_negative_windows)]


        if np.min(negative_diffs) < np.min(positive_diffs):
            fits = negative_fits
            diffs = negative_diffs
        else:
            fits = positive_fits
            diffs = positive_diffs
        muon_mass = 105.6583755  # MeV
        muon_momentum = 125  # MeV
        positron_mass = 0.5  # MeV
        transverse_momentum1, vertical_momentum1, _ = compute_momentum(r_func, lambda z: fit_function_params(z, fits[0]))
        unrealistic1 = np.sqrt(
            vertical_momentum1 ** 2 + transverse_momentum1 ** 2) > muon_mass + muon_momentum - positron_mass
        transverse_momentum2, vertical_momentum2, _ = compute_momentum(r_func,  lambda z: fit_function_params(z, fits[-1]))
        unrealistic2 = np.sqrt(
            vertical_momentum2 ** 2 + transverse_momentum2 ** 2) > muon_mass + muon_momentum - positron_mass

        if unrealistic1 and unrealistic2:
            return False, "Error: An impossible momentum result was found."
        elif unrealistic1 and not unrealistic2:
            fit = fits[-1]
        elif unrealistic2 and not unrealistic1:
            fit = fits[0]
        else:
            if diffs[-1] < diffs[0]:
                fit = fits[-1]
            else:
                fit = fits[0]

        positive_fit_evaluated = positive_fits_evaluated[0]
        negative_fit_evaluated = negative_fits_evaluated[0]

    fit_function = lambda z: fit_function_params(z, fit)
    return True, [fit, fit_function, positive_fit_evaluated, negative_fit_evaluated, det_phi_positive_slope,
                  det_phi_negative_slope]


def compute_momentum(r_func, phase_fit_function):
    transverse_momentum = 1 / 3.33564 * r_func(0) / 1000 * 1 * 3 * 1000  # MeV

    dz = 1e-1  # numerical derivative interval
    phase_velocity = (phase_fit_function(dz) - phase_fit_function(0)) / dz

    vertical_momentum = 1 / phase_velocity / r_func(0) * transverse_momentum

    # determine the direction
    if vertical_momentum > 0:
        direction = 1
    else:
        direction = -1

    return transverse_momentum, vertical_momentum, direction


def positron_track(tree: rt.TTree, entry_index: int, plotme: bool = False, pixelsize: float = 0.1,
                   variable_radius = True, variable_center = True, variable_phase_velocity = True) -> dict:
    """
    Fits a helix to the positron trajectory given arrays of detector hit coordinates.

    :param tree: The tree containing the detector information
    :param entry_index: The entry for which the positron is tracked
    :param plotme: Flag which specifies if a preview plot should be generated.
    :param pixelsize: The size of the pixels of the detector in mm.
    :param variable_radius: Flag which specifies whether a changing radius (as linear function of z) should be used.
    :param variable_center: Flag which specifies whether a changing circle center (as linear function of z) should be
                            used.
    :param variable_phase_velocity: Flag which specifies whether a changing phase velocity (exponential function)
                                    should be used.

    :return: A dictionary containing the fit parameters, transverse/vertical momentum, the direction of the positron
             and other information.
    """

    # Get relevant detector information
    tree.GetEntry(entry_index)
    det_x = np.array(tree.det_x)
    det_y = np.array(tree.det_y)
    det_z = np.array(tree.det_z)
    det_ID = np.array(tree.det_ID)
    det_time_start = np.array(tree.det_time_start)

    # remove consecutive detector hits that are too close
    consecutive_distance = np.sqrt((det_x[1:] - det_x[:-1])**2 + (det_y[1:] - det_y[:-1])**2 + (det_z[1:] - det_z[:-1])**2)
    remove = np.array([False] + list(consecutive_distance < np.ones_like(consecutive_distance)*2))
    det_x = det_x[np.logical_not(remove)]
    det_y = det_y[np.logical_not(remove)]
    det_z = det_z[np.logical_not(remove)]
    det_ID = det_ID[np.logical_not(remove)]

    max_z = np.max(det_z)
    min_z = np.min(det_z)

    unknown_result = {"direction": 0, "transverse_momentum": 0, "vertical_momentum": 0,
                      "transverse_momentum_error": 100, "vertical_momentum_error": 100, "time": 0,
                      "transverse_direction": 0,
                      "r_slope": 0, "r_0": 0, "x_c_slope": 0, "x_c_0": 0,  "y_c_slope": 0, "y_c_0": 0,
                      "u": 0, "phi_0": 0, "fig": None, "max_z": max_z, "min_z": min_z}

    if det_time_start.size > 0:
        det_time = det_time_start[0]
    else:
        det_time = 0

    # sort according to z coordinate
    sorted_idx = np.argsort(det_z)
    det_x = det_x[sorted_idx]
    det_y = det_y[sorted_idx]
    det_z = det_z[sorted_idx]
    det_ID = det_ID[sorted_idx]

    # DETECTOR ROUNDING
    det_x, det_y, det_z = pixel_rounding(det_x, det_y, det_z, pixelsize)

    # CIRCLE FITTING
    success, params = circle_fitting(det_x, det_y, det_z, det_ID, variable_radius=variable_radius,
                                     variable_center=variable_center)
    if not success:
        unknown_result.update({"message": params})
        return unknown_result
    else:
        r_func, x_c_func, y_c_func, r_params, x_c_params, y_c_params, circle_zs = params

    ### DETERMINING VERTICAL MOMENTUM
    success, params = phase_fitting(det_x, det_y, det_z, r_func, x_c_func, y_c_func,
                                    variable_phase_velocity=variable_phase_velocity)
    if success:
        fit, fit_function, positive_fit_evaluated, negative_fit_evaluated, det_phi_positive_slope, det_phi_negative_slope = params
    else:
        unknown_result.update({"message": params})
        return unknown_result

    # compute transverse and vertical momentum.
    transverse_momentum, vertical_momentum, direction = compute_momentum(r_func, fit_function)

    # compare with actual momentum
    actual_transverse_momentum = np.sqrt(tree.posIniMomX ** 2 + tree.posIniMomY ** 2)
    transverse_error = np.abs((transverse_momentum - actual_transverse_momentum) / actual_transverse_momentum * 100)

    actual_vertical_momentum = tree.posIniMomZ
    vertical_error = np.abs((vertical_momentum - actual_vertical_momentum) / actual_vertical_momentum * 100)

    """
    # find the optimal phi (the phi_0 from the linear fit is too imprecise)
    # this does not affect transverse/vertical momentum, and is therefore only done for the sake of plotting
    u = fit[0]
    phis = np.linspace(0, 2 * np.pi, 100)
    phi_residues = [helix_residue([r_func(det_z), x_c_func(det_z), y_c_func(det_z), u, phi_1],
                                  det_x, det_y, det_z) for phi_1 in phis]
    phi_0 = phis[np.argmin(phi_residues)]
    fit[1] = phi_0
    fit_function = lambda x: fit[0]*x + fit[1]"""

    # compute whether the positron was moving inwards our outwards angle with respect to orbit
    muon_orbit_radius = 140  # mm

    # compute intersection of helix with muon orbit
    # solve (a+r * cos(theta))^2 + (b + r *sin(theta))^2 = m for theta
    func = lambda z: (x_c_func(z) + r_func(z)*np.cos(fit_function(z))) ** 2\
                     + (y_c_func(z) + r_func(z)*np.sin(fit_function(z))) **2 - muon_orbit_radius**2

    global failcount

    converged = False
    offset = 1
    z=None
    while not converged:
        z_plus, results_pos = scipy.optimize.newton(func, offset, full_output=True, disp=False)
        z_min, results_neg = scipy.optimize.newton(func, offset, full_output=True, disp=False)

        zs = np.array([z_plus, z_min]).reshape(-1)[np.array([results_pos.converged, results_neg.converged])]
        z = 0
        if zs.size != 0 and np.min(np.abs(zs)) < 20:
            z = zs[np.argmin(np.abs(zs))]
            converged = True

        if offset > 10:
            converged = False
            break
        offset += 1

    if converged:
        theta = np.arctan2(y_c_func(z), x_c_func(z))
        x = x_c_func(z) + r_func(z) * np.cos(fit_function(z))
        y = y_c_func(z) + r_func(z) * np.sin(fit_function(z))
        alpha = fit_function(z)
        alpha = np.arctan2(y, x)

        if np.sin(alpha - theta) < 0:  # -np.pi/2 + alpha <= theta < np.pi/2 + alpha
            transverse_direction = 1
        elif np.sin(alpha - theta) > 0:
            transverse_direction = -1
        else:
            failcount += 1
            transverse_direction = 0

        if np.abs(2*np.pi / fit[0]) < 30:
            failcount += 1
            transverse_direction = 0
    else:
        failcount += 1
        transverse_direction = 0


    if entry_index % 1000 == 0:
        print(failcount)
    # check if this can be a physical trajectory
    muon_mass = 105.6583755  # MeV
    muon_momentum = 125  # MeV
    positron_mass = 0.5  # MeV
    unrealistic = np.sqrt(vertical_momentum**2 + transverse_momentum**2) > muon_mass + muon_momentum - positron_mass

    if plotme:
        fig = plot_helix(det_x, det_y, det_z, r_func, x_c_func, y_c_func, tree, circle_zs, det_phi_positive_slope,
                         det_phi_negative_slope, variable_phase_velocity, positive_fit_evaluated, negative_fit_evaluated, fit,
                         fit_function, direction=np.sign(vertical_momentum))
        plt.savefig("graphs/success"+str(entry_index)+".png")
    else:
        fig = None
    if unrealistic:
        plot_helix(det_x, det_y, det_z, r_func, x_c_func, y_c_func, tree, circle_zs, det_phi_positive_slope,
                   det_phi_negative_slope, variable_phase_velocity, positive_fit_evaluated, negative_fit_evaluated, fit,
                   fit_function)
        plt.savefig("graphs/error"+str(entry_index)+".png")
        unknown_result.update({"message": "Error: An impossible momentum result was found."})
        return unknown_result

    if np.abs(np.arctan2(vertical_momentum, transverse_momentum)) < 0.001:
        return {"direction": 0, "transverse_momentum": transverse_momentum,
                "vertical_momentum": vertical_momentum,
                "transverse_momentum_error": transverse_error, "vertical_momentum_error": vertical_error,
                "time": det_time, "transverse_direction": transverse_direction,
                "r_slope": r_params[0], "r_0": r_params[1], "x_c_slope": x_c_params[0], "x_c_0": x_c_params[1],
                "y_c_slope": y_c_params[0], "y_c_0": y_c_params[1],
                "phase_fit_params": fit, "phase_fit_func": fit_function,
                "message": "Success, but positron has small vertical momentum; direction set to unknown.", "fig": fig,
                "max_z": max_z, "min_z": min_z}
    return {"direction": direction, "transverse_momentum": transverse_momentum, "vertical_momentum": vertical_momentum,
            "transverse_momentum_error": transverse_error, "vertical_momentum_error": vertical_error, "time": det_time,
            "transverse_direction" : transverse_direction,
            "r_slope": r_params[0], "r_0": r_params[1], "x_c_slope": x_c_params[0], "x_c_0": x_c_params[1],
            "y_c_slope": y_c_params[0], "y_c_0": y_c_params[1],
            "phase_fit_params": fit, "phase_fit_func": fit_function,
            "message": "Success", "fig": fig, "max_z": max_z, "min_z": min_z}