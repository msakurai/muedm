import ROOT as rt
import numpy as np
import root_numpy as rn
import os
import matplotlib.pyplot as plt

# Plots the following momentum spectra:
#  - the true momentum spectrum of all events (run with "storeOnlyEventsWithHits false" in musrSim!)
#  - the true momentum spectrum of events with at least one detector hit   (perhaps this should be changed to 3 hits)
#  - the detected momentum spectrum

rt.gStyle.SetOptStat(0)
rt.gROOT.SetBatch(1)

run_number = "200"  # run without E-field
label = "pixelanalysis"

if label != "":
    label = "_" + label

raw_data_file = "raw_data/musr_" + str(run_number) + "_.root"
processed_data_file = 'processed_data/pixel_data_' +  str(run_number) + label + '.root'

tempfile_name = "processed_data/tempfile_tobedelected.root"  # temp file name for momentum root tree
if __name__ == '__main__':
    t1 = rt.TChain("t1")
    t1.Add(raw_data_file)
    t_bin = 150
    t_min = 1
    t_max = 150

    # import processed data tree
    input_file = rt.TFile(processed_data_file, 'READ')
    tree = input_file.Get("px0nm")
    tree.GetEntry(0)

    # allows t1.Draw to access elements of tree.
    t1.AddFriend(tree)

    # true momentum spectrum
    canvas = rt.TCanvas("canvas", "Momentum Spectrum; Momentum (MeV/c); Counts", 1000, 500)
    momentum_histogram = rt.TH1F("momentum_histogram", "Momentum Spectrum; Momentum (MeV/c); Counts", t_bin, t_min,
                                 t_max)
    t1.Draw("sqrt(posIniMomX**2 + posIniMomY**2 + posIniMomZ**2)>>momentum_histogram")
    momentum_histogram.Draw()
    canvas.Draw()
    canvas.SaveAs("graphs/momentum_spectrum_true_all_" + str(run_number) + ".png")

    del momentum_histogram
    del canvas

    # only considering hits, but with true initial momentum
    detected = rn.tree2array(tree, branches="detected")[0]
    momenta_x = rn.tree2array(t1, branches="posIniMomX")
    momenta_y = rn.tree2array(t1, branches="posIniMomY")
    momenta_z = rn.tree2array(t1, branches="posIniMomZ")
    momenta = np.sqrt(momenta_x ** 2 + momenta_y ** 2 + momenta_z ** 2)
    assert momenta.size == detected.size, "Mismatch between raw and processed tree sizes!"

    electron_mass = 0.5
    p_max = 144

    gamma = lambda p: np.sqrt(p ** 2 + electron_mass ** 2) / electron_mass
    y0 = lambda p: (np.sqrt(electron_mass**2 + p**2) - electron_mass) / (np.sqrt(electron_mass**2 + p_max**2) - electron_mass)
    asymmetry = lambda p: (-8 * y0(p) ** 2 + y0(p) + 1) / (4 * y0(p) ** 2 - 5 * y0(p) - 5)
    plt.plot(np.linspace(0,144,1000), asymmetry(np.linspace(0,144,1000)))
    plt.savefig("test32")
    print(momenta)
    print(asymmetry(momenta))
    total_asymmetry = np.mean(asymmetry(momenta))
    print(total_asymmetry)

    nEntries = detected.size

    # create temporary root file, otherwise ROOT throws an error.
    outputFile = rt.TFile(tempfile_name, 'RECREATE')
    my_tree = rt.TTree("momentum_tree", "")
    my_tree.Branch("momentum", momenta, "momentum[" + str(nEntries) + "]/D")
    my_tree.Branch("detected", detected, "detected[" + str(nEntries) + "]/I")
    my_tree.Fill()
    outputFile.Write()

    canvas = rt.TCanvas("canvas", "Momentum Spectrum; Momentum (MeV/c); Counts", 1000, 500)
    momentum_histogram = rt.TH1F("momentum_histogram", "Momentum Spectrum; Momentum (MeV/c); Counts", t_bin, t_min,
                                 t_max)
    my_tree.Draw("momentum>>momentum_histogram", "detected > 0.5")

    momentum_histogram.Draw()

    canvas.Draw()
    canvas.SaveAs("graphs/momentum_spectrum_true_hitsonly_" + str(run_number) + ".png")
    outputFile.Close()

    os.remove(tempfile_name)  # remove the temporary file
    del momentum_histogram
    del canvas

    momenta_x = rn.tree2array(tree, branches="initial_momentum_x")
    momenta_y = rn.tree2array(tree, branches="initial_momentum_y")
    momenta_z = rn.tree2array(tree, branches="initial_momentum_z")
    momenta = np.sqrt(momenta_x ** 2 + momenta_y ** 2 + momenta_z ** 2)
    momenta = momenta[np.logical_not(np.isnan(momenta))]
    print(momenta)
    print(asymmetry(momenta))
    total_asymmetry = np.mean(asymmetry(momenta))
    print(total_asymmetry)

    # only considering hits, but with detected initial momentum
    canvas = rt.TCanvas("canvas", "Momentum Spectrum; Momentum (MeV/c); Counts", 1000, 500)
    momentum_histogram = rt.TH1F("momentum_histogram", "Momentum Spectrum; Momentum (MeV/c); Counts", t_bin, t_min,
                                 t_max)
    tree.Draw("sqrt(initial_momentum_x**2 + initial_momentum_y**2 + initial_momentum_z**2)>>momentum_histogram")
    momentum_histogram.Draw()
    canvas.Draw()
    canvas.SaveAs("graphs/momentum_spectrum_detected_" + str(run_number) + ".png")

    del momentum_histogram
    del canvas

    t_bin = 150
    t_min = 1
    t_max = 150

    canvas = rt.TCanvas("canvas", "Momentum Spectrum; Momentum (MeV/c); Counts", 1000, 500)

    momentum_histogram = rt.TH1F("momentum_histogram", "Momentum Spectrum; Momentum (MeV/c); Counts", t_bin, t_min, t_max)
    tree.Draw("sqrt(transverse_momentum**2 + vertical_momentum**2)>>momentum_histogram")
    momentum_histogram.Draw()
    canvas.Draw()
    canvas.SaveAs("graphs/momentum_spectrum_" + str(run_number) + ".png")



