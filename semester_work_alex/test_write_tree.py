import ROOT as rt
import numpy as np

if __name__ == "__main__":
    pixel_sizes = 10 ** (np.linspace(-3, 1, 20))
    pixel_tree_names = ["px=" + str(int(pixel_size * 1000000)) + "nm" for pixel_size in pixel_sizes]
    outputFile = rt.TFile('pixeltrees3.root', 'RECREATE')

    tree_list = []
    for i,pixel_size in enumerate(pixel_sizes):
        my_tree = rt.TTree(pixel_tree_names[i], "")
        tree_list.append(my_tree)

        transverse_momentum = np.array([1,2,3],dtype=np.float64)
        vertical_momentum = np.array([4,5,6],dtype=np.float64)
        transverse_momentum_error = np.array([0.1,0.2,0.12], dtype=np.float64)
        vertical_momentum_error = np.array([0.05, 0.02, 0.1], dtype=np.float64)
        direction = np.array([1,0,-1], dtype=np.int32)
        nEntries = direction.size

        my_tree.Branch("transverse_momentum", transverse_momentum, "transverse_momentum["+str(nEntries)+"]/D")
        my_tree.Branch("vertical_momentum", vertical_momentum, "vertical_momentum["+str(nEntries)+"]/D")
        my_tree.Branch("transverse_momentum_error", transverse_momentum_error, "transverse_momentum_error["+str(nEntries)+"]/D")
        my_tree.Branch("vertical_momentum_error", vertical_momentum_error, "vertical_momentum_error["+str(nEntries)+"]/D")
        my_tree.Branch("direction", direction, "direction["+str(nEntries)+"]/I")
        my_tree.Fill()
    outputFile.Write()
    outputFile.Close()