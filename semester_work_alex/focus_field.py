import numpy as np


def focus_field(r, z):
    return np.array([-r * z, -0.5*r**2 + z**2])


if __name__ == '__main__':
    field_normalization = 0.005 / np.sqrt(np.sum(focus_field(140, 50)**2))

    output = open("raw_data/focus_field.txt", "w")

    nr, nz = 500, 500

    rs = np.linspace(0, 300, nr)
    zs = np.linspace(-100, 100, nz)
    output.write(str(nr) + " " + str(nz) + " " +"mm" + " " + str(field_normalization) + "\n")
    for i, r in enumerate(rs):
        print(i)
        for z in zs:
            Br, Bz = focus_field(r, z)
            out_line = str(r) + " " + str(z) + " " + str(Br) + " " + str(Bz)
            output.write(out_line + "\n")
    output.close()
