

# Trajectory Tracking  `trajectory_tracking.py`
The python script `trajectory_tracking.py` uses the helix fitting algorithm implemented in the Python class `Helix` (found in `utils/Helix.py`, more information below) to determine <br>
the trajectories of the released positrons. Various key parameters, such as the transverse and vertical momentum, are extracted.

This is done for various pixel sizes which will be used in subsequent data analysis.<br>
These are stored in a root file in the directory `processed_data`. The data for each pixel size are stored in a different root tree.<br>
The root trees are named as `px<pixel_size>nm`, where `<pixel_size>` is the size of the pixel, in nm, rounded down.

The exact parameters stored in the root tree are
* `pixel_size`: The exact size of the pixel in mm.
* `vertical_direction`: The measured vertical release direction of the positron
* `radial_direction`: The measured radial direction of the positron.
* `actual_vertical_direction`: The true vertical release direction of the positron
* `actual_radial_direction`: The true radial direction of the positron
* `vertical_momentum`: The measured (initial) vertical momentum of the positron
* `transverse_momentum`: The measured (initial) transverse momentum of the positron
* `time`: The time of the first detector hit, in microseconds.
* `initial_momentum_x`: The initial momentum in the x-direction given by the positron tracking algorithm.
* `initial_momentum_y`: The initial momentum in the y-direction given by the positron tracking algorithm.
* `initial_momentum_z`: The initial momentum in the z-direction given by the positron tracking algorithm.
* `initial_position_x`: The initial x-position given by the positron tracking algorithm.
* `initial_position_y`: The initial y-position given by the positron tracking algorithm.
* `initial_position_z`: The initial z-position given by the positron tracking algorithm.

An additional root tree under the name of `ref_tree` is stored in the file, which contains information for pure up-down detection (without pixel detectors).<br>
The contents of this tree are
* `vertical_direction`: The measured direction of the positron
* `time`: The time of the first detector hit
* `max_z`: The maximum z coordinate of all detector hits for a given event
* `min_z`: The minimum z coordinate of all detector hits for a given event

NOTE: This script must be run first to process new root files, before the subsequent analysis can be run. 

# Pixel Analysis  `pixel_analysis.py`
This uses the `pixel_rounding` function (in `utils/detector.py`) to round detector hits into various bins. 
The dependence of the momentum error, direction error, accuracy and efficiency on the pixel size is then plotted against the pixel resolution (for all resolutions computed in `trajectory_tracking.py`)

# g-2 Precession Analysis `g-2_precession.py`
The detected radial direction is used along with the time information to measure the in-out asymmetry due to g-2 precession. 

The precession frequency can be used to determine the electric field required to satisfy the frozen-spin condition, <br>whereas
the amplitude of the asymmetry signal is required to be able to compute the muon EDM from only the slope of the asymmetry signal.

The Boolean flag `use_exact_vertex` denotes whether to use the positron tracking vertex or the exact decay vertex.

# muon EDM Time Analysis  `time_analysis_muEDM.py`
The muon EDM is calculated for various time bin sizes, and the error is plotted against the time resolution.

# g-2 Time Analysis  `time_analysis_g-2.py`
The anomalous magnetic moment is calculated for various time bin sizes, and the error is plotted against the time resolution.

# Positron Tracking: The `Helix` Class `utils/Helix.py`
![](example_plot.png)
The positron tracking is implemented in the python class `Helix`.

The method begins by fitting a circle to the x- and y-coordinates of the detector hits. If the flags `variable_radius` and/or `variable_center`<br>
are set, the circles are fitted as a function of the vertical coordinate z. This is to approximate scattering effects.

`variable_radius`: The radius is given by an exponential function `r(z) = r_0 * exp(-b*z)`<br>
`variable_center`: The center coordinates x and y are given by affine functions.

Afterwards, the phase is computed for each detector hit with respect to the previously computed center coordinates `(x_c, y_c)`.<br>
This is given by `alpha = arctan2(y-y_c, x-x_c)` where `x,y` are the detector hits.

The phase is monotonized in both directions, positive and negative, and fitted with a linear fit. Whichever fits the data better <br> 
(least squares) is taken as the direction. <br>
If the flag `variable_phase_velocity` is set, the linear fits are done on smaller intervals, which are larger than one rotation.<br>
The direction where the minimum least square difference over all intervals is the smallest is chosen.

This is done such that the phase velocity can change, but if there is a periodic non-linearity (i.e. we monotonized the wrong way) it will still be detected.

NOTE: Using these variable functions instead of constants appears to have negligible improvements to the accuracy.

## Detector Rounding
Detector related functions are implemented in `detector.py`.
### Pixel rounding
The function `pixel_rounding` implements a pixel rounding method for cylindrical detectors. Note that this may not work
for other detector detector geometries, as the function implicitly uses cylindrical coordinates for the rounding
process.

### Time Rounding
The time resolution has been reduced by simply increasing the bin size of the histograms.

# Simulations
Below are several simulations with varying parameters. All of them have 100000 muons.

### Simulations without scattering
`100.mac`: EDM = 0, 100000 events,  with 0.5 degrees divergencee, no scattering <br>
For g-2 precession

`110.mac`: EDM = 1.8e-18, 100000 events,  with 0.5 degrees divergencee, no scattering <br>
For linear fit muon EDM calculation

`120.mac`: EDM = 1.8e-17, 100000 events,  with 0.5 degrees divergencee, no scattering <br>
For sinusoidal fit muon EDM calculation

### Simulations with scattering
`200.mac`: EDM = 0, 100000 events,  with 0.5 degrees divergencee, with scattering <br>
For g-2 precession

`210.mac`: EDM = 1.8e-18, 100000 events,  with 0.5 degrees divergencee, with scattering <br>
For linear fit muon EDM calculation

`220.mac`: EDM = 1.8e-17, 100000 events,  with 0.5 degrees divergencee, with scattering <br>
For sinusoidal fit muon EDM calculation

`320.mac`: EDM = 1.8e-18, 100000 events,  with 0.5 degrees divergencee, with scattering <br>
For sinusoidal fit muon EDM calculation, with only two cylinders as detectors

## Older Simulations
### Scattering and Divergence
`10.mac`: No EDM, no divergence, no scattering <br>
This is primarily for testing the positron tracking. The positrons should follow helical paths since no scattering occurs.

`11.mac`: EDM = 2e-18, no divergence, no scattering<br>
Perfect situation. The beam is perfectly focussed and not subject to systematic errors.

`12.mac`: EDM = 2e-18, with 0.3 degrees divergence and scattering<br>
Realistic situation without systematic errors.

`13.mac`: EDM = 2e-18, with 0.1 degrees and 2cm divergence and scattering<br>
Realistic situation without systematic errors.

### Systematic Errors
`20.mac`: EDM = 2e-18, without divergence and scattering, and a vertical offset of the storage ring from the center by 2cm. <br>
This will not cause betatron oscillations, but can create a bias in the detection mechanism.

`21.mac`: EDM = 2e-18, without divergence and scattering, and a vertical offset of the initial beam position of 2cm. <br>
This will cause betatron oscillations which could potentially be measured in the signal.

`22.mac`: EDM = 2e-18, with a spatial divergence of 2cm, and a vertical offset of the initial beam position of 2cm. <br>
This will cause betatron oscillations which could potentially be measured in the measured signal.


### Alternate detector setup
`32.mac`: EDM = 2e-18, with 0.3 degrees divergence and scattering<br>
Same as `10.mac`, but with added plane detectors extending radially outwards between the inner and outer detector cylinder.
