import ROOT as rt
import numpy as np
import matplotlib.pyplot as plt
from utils.tree_tools import get_list_of_treenames
from utils.detector import get_detect_info
import math

rt.gStyle.SetOptStat(0)
rt.gROOT.SetBatch(1)

run_number = 100
label = "pixelanalysis"
time_resolution = 0.1  # us

if label != "":
    label = "_" + label

processed_data_file = 'processed_data/pixel_data_' + str(run_number) + label + '.root'
raw_data_file = 'raw_data/musr_' + str(run_number) + '_.root'

if __name__ == '__main__':

    # import raw data tree
    t1 = rt.TChain('t1')
    t1.Add(raw_data_file)

    # Compute the actual directions of the positrons from the initial momentum.
    num_entries = t1.GetEntries()
    actual_vertical_direction = []
    actual_radial_direction = []

    for entry_index in range(0, num_entries):
        t1.GetEntry(entry_index)
        actual_vertical_momentum = t1.posIniMomZ

        if actual_vertical_momentum > 0:
            actual_vertical_direction.append(1)
        else:
            actual_vertical_direction.append(-1)
        actual_radial_direction.append(np.sign(t1.muDecayPosX * t1.posIniMomX + t1.muDecayPosY * t1.posIniMomY))

    # import processed data trees
    input_file = rt.TFile(processed_data_file, 'READ')
    treenames = get_list_of_treenames(processed_data_file)
    treenames.remove("ref_tree")

    trees = [input_file.Get(treename) for treename in treenames]
    print(trees)
    tmin = 0  # lower time limit for histogram, us
    tmax = 25  # upper time limit for histogram, us
    tbin = int((tmax - tmin) / time_resolution)  # number of bins

    pixel_sizes = []
    muEDMs = []
    muEDMerrors = []
    accuracies = []
    efficiencies = []
    biases = []
    transverse_accuracies = []
    transverse_efficiencies = []
    transverse_biases = []
    total_angle_errors = []
    horizontal_angle_errors = []
    vertical_angle_errors = []
    transverse_momentum_errors = []
    vertical_momentum_errors = []

    for tree in trees:
        tree.GetEntry(0)
        pixel_sizes.append(tree.pixel_size)

        # get accuracy, efficiency and bias from direction arrays.
        print("Vertical Direction:")
        detect_info_dict = get_detect_info(tree.vertical_direction, actual_vertical_direction, print_info=True)
        accuracy = detect_info_dict["accuracy"]
        efficiency = detect_info_dict["efficiency"]
        bias = detect_info_dict["bias"]
        print("Radial Direction:")
        transverse_detect_info_dict = get_detect_info(tree.radial_direction, actual_radial_direction, print_info=True)
        transverse_accuracy = transverse_detect_info_dict["accuracy"]
        transverse_efficiency = transverse_detect_info_dict["efficiency"]
        transverse_bias = transverse_detect_info_dict["bias"]

        # compute average momentum errors
        labelled = np.array(tree.vertical_direction) != np.zeros_like(tree.vertical_direction)

        # append results to arrays
        accuracies.append(accuracy)
        efficiencies.append(efficiency)
        biases.append(bias)
        transverse_accuracies.append(transverse_accuracy)
        transverse_efficiencies.append(transverse_efficiency)
        transverse_biases.append(transverse_bias)

        # Compute the error in the direction and magnitude of the initial momentum
        total_angle_error = []
        vertical_angle_error = []
        horizontal_angle_error = []

        vertical_momentum_error = []
        transverse_momentum_error = []
        for entry_index in range(0, num_entries):
            t1.GetEntry(entry_index)

            actual_initial_momentum = np.array([t1.posIniMomX, t1.posIniMomY, t1.posIniMomZ])
            initial_momentum = np.array([tree.initial_momentum_x[entry_index],
                                         tree.initial_momentum_y[entry_index],
                                         tree.initial_momentum_z[entry_index]])

            # determine the angle error with respect to the polar angle
            vertical_angle_proj = lambda vector: np.array([np.sqrt(vector[0]**2 + vector[1]**2), vector[2]])
            vec1 = vertical_angle_proj(actual_initial_momentum)
            vec2 = vertical_angle_proj(initial_momentum)
            vertical_angle = np.arccos(np.dot(vec1, vec2) / np.sqrt(np.sum(vec1**2)) / np.sqrt(np.sum(vec2**2)))
            if not np.isnan(vertical_angle):
                vertical_angle_error.append(vertical_angle)

            # determine the angle error with respect to the azimuthal angle
            horizontal_angle_proj = lambda vector: np.array([vector[0], vector[1]])
            vec1 = horizontal_angle_proj(actual_initial_momentum)
            vec2 = horizontal_angle_proj(initial_momentum)
            azimuthal_angle = np.arccos(np.dot(vec1, vec2) / np.sqrt(np.sum(vec1 ** 2)) / np.sqrt(np.sum(vec2 ** 2)))
            if not np.isnan(azimuthal_angle):
                horizontal_angle_error.append(azimuthal_angle)

            # determine the total angle error
            angle = np.arccos(np.dot(actual_initial_momentum, initial_momentum)  \
                              / np.sqrt(np.sum(actual_initial_momentum**2)) / np.sqrt(np.sum(initial_momentum**2)))
            if not np.isnan(angle):
                total_angle_error.append(angle)

            # compute the error of the transverse and vertical momentum
            transverse_momentum = np.sqrt(np.sum((initial_momentum[:-1])**2))
            actual_transverse_momentum = np.sqrt(np.sum((actual_initial_momentum[:-1])**2))

            temp = np.abs(transverse_momentum - actual_transverse_momentum) / np.sqrt(np.sum(actual_initial_momentum**2))
            if not np.isnan(temp):
                transverse_momentum_error.append(temp)
            temp = np.abs(initial_momentum[2] - actual_initial_momentum[2]) / np.sqrt(np.sum(actual_initial_momentum**2))
            if not np.isnan(temp):
                vertical_momentum_error.append(temp)

        total_angle_errors.append(np.median(total_angle_error) * 180 / np.pi)
        vertical_angle_errors.append(np.median(vertical_angle_error) * 180 / np.pi)
        horizontal_angle_errors.append(np.median(horizontal_angle_error) * 180 / np.pi)
        transverse_momentum_errors.append(np.median(transverse_momentum_error)*100)
        vertical_momentum_errors.append(np.median(vertical_momentum_error)*100)

    plt.figure(figsize=(9,8))
    plt.subplot(2, 1, 1)
    plt.loglog(pixel_sizes, vertical_momentum_errors)
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Median Vertical Momentum Error (%)")
    plt.grid(which="both")

    ycoords = vertical_momentum_errors
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    print(ymin, ymax)
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.subplot(2, 1, 2)
    plt.loglog(pixel_sizes, transverse_momentum_errors)
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Median Horizontal Momentum Error (%)")
    plt.grid(which="both")

    ycoords = transverse_momentum_errors
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = np.math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.savefig("graphs/pixel_analysis_momenta_"+str(run_number)+label+".png")

    plt.figure(figsize=(9, 12))
    plt.subplot(3, 1, 1)
    plt.loglog(pixel_sizes, np.array(vertical_angle_errors) * np.pi/180 * 1000)
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Median Zenith Angle Error (mrad)")
    plt.grid(which="both")

    ycoords = np.array(vertical_angle_errors) * np.pi/180 * 1000
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.subplot(3, 1, 2)
    plt.loglog(pixel_sizes, np.array(horizontal_angle_errors) * np.pi/180 * 1000)
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Median Azimuthal Angle Error (mrad)")
    plt.grid(which="both")

    ycoords = np.array(horizontal_angle_errors) * np.pi/180 * 1000
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.subplot(3, 1, 3)
    plt.loglog(pixel_sizes, np.array(total_angle_errors) * np.pi/180 * 1000)
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Median Total Angle Error (mrad)")
    plt.grid(which="both")

    ycoords = np.array(total_angle_errors) * np.pi/180 * 1000
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.savefig("graphs/pixel_analysis_direction_" + str(run_number) + label +".png")

    plt.figure(figsize=(9, 8))
    plt.subplot(2, 1, 1)
    plt.loglog(pixel_sizes, np.abs(100-np.array(accuracies)))
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Vertical Labelling Inaccuracy (%)")
    plt.grid(which="both")

    ycoords = np.abs(100-np.array(accuracies))
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.subplot(2, 1, 2)
    plt.loglog(pixel_sizes, np.abs(100-np.array(transverse_accuracies)))
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Radial Labelling Inaccuracy (%)")
    plt.grid(which="both")

    ycoords = np.abs(100-np.array(transverse_accuracies))
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.savefig("graphs/pixel_analysis_accuracy_" + str(run_number) + label +".png")

    plt.figure(figsize=(9, 8))
    plt.subplot(2, 1, 1)
    plt.loglog(pixel_sizes, np.abs(100-np.array(efficiencies)))
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Vertical Labelling Inefficiency (%)")
    plt.grid(which="both")

    ycoords = np.abs(100-np.array(efficiencies))
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.subplot(2, 1, 2)
    plt.loglog(pixel_sizes, np.abs(100-np.array(transverse_efficiencies)))
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Radial Labelling Inefficiency (%)")
    plt.grid(which="both")

    ycoords = np.abs(100-np.array(transverse_efficiencies))
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.savefig("graphs/pixel_analysis_efficiency_" + str(run_number) + label + ".png")

    plt.figure(figsize=(9, 8))
    plt.subplot(2, 1, 1)
    plt.loglog(pixel_sizes, np.abs(biases))
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("Vertical Labelling Bias (%)")
    plt.grid(which="both")

    ycoords = np.abs(biases)
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.subplot(2, 1, 2)
    plt.loglog(pixel_sizes, np.abs(transverse_biases))
    plt.xlabel("Pixel Size (mm)")
    plt.ylabel("graphs/Radial Labelling Bias (%)")
    plt.grid(which="both")

    ycoords = np.abs(transverse_biases)
    ycoords = np.abs(ycoords)
    ycoords = ycoords[ycoords > 0]
    ymin, ymax = math.floor(np.log10(np.min(ycoords))), math.ceil(
        np.log10(np.max(ycoords)))
    plt.ylim(10 ** ymin, 10 ** ymax)

    plt.savefig("graphs/pixel_analysis_bias_" + str(run_number) + label +".png")