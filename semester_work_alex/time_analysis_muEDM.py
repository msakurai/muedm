import ROOT as rt
import numpy as np
import matplotlib.pyplot as plt
from utils.positron_tracking import positron_track
from random import randint
from utils.tree_tools import add_branch2tree, get_list_of_treenames
from scipy.constants import c

rt.gStyle.SetOptStat(0)
rt.gROOT.SetBatch(1)

run_number = 320
label = "0px"

save_asymmetry_graphs = False
use_exact_vertex = False  # specifies whether to use exact decay vertex or positron tracking
mode = "sinusoidal"  # "sinusoidal" or "linear" fit
actual_EDM = 1.8e-17

resolutions = 10**(np.linspace(-4,1.5,300))

if label != "":
    label = "_" + label

# label which specifies whether the helix fitting algorithm or the exact decay vertex was used.
# appended to the filename of the plots
exact_label = "tracking"
if use_exact_vertex:
    exact_label = "exact"

input_data_file = 'processed_data/pixel_data_'+str(run_number)+label+'.root'
raw_data_file = 'raw_data/musr_' + str(run_number) + '_.root'

# import processed data trees
input_file = rt.TFile(input_data_file, 'READ')
treenames = get_list_of_treenames(input_data_file)
print("trees in root file: ", treenames)
tree = input_file.Get("px0nm")

# import raw data tree
t1 = rt.TChain('t1')
t1.Add(raw_data_file)

if __name__ == '__main__':
    tmin = 0  # lower time limit for histogram, us
    tmax = 25  # upper time limit for histogram, us

    pixel_sizes = []
    muEDMs = []
    muEDMerrors = []
    accuracies = []
    efficiencies = []

    for time_resolution in resolutions:
        tbin = int((tmax - tmin) / time_resolution)  # number of bins

        canvas1 = rt.TCanvas("canvas1", "Asymmetry Plot", 1000,500)
        up_histogram = rt.TH1F("up_histogram","", tbin, tmin, tmax)
        down_histogram = rt.TH1F("down_histogram","", tbin, tmin, tmax)
        up_histogram.Sumw2()
        down_histogram.Sumw2()

        if not use_exact_vertex:
            tree.Draw("time >> up_histogram", "vertical_direction > 0.5 && radial_direction != 0", "gOff")
            tree.Draw("time >> down_histogram", "vertical_direction < -0.5 && radial_direction != 0", "gOff")
        else:
            t1.Draw("muDecayTime >> up_histogram", "posIniMomZ  > 0", "gOff")
            t1.Draw("muDecayTime >> down_histogram", "posIniMomZ  < 0", "gOff")

        up_down_diff = up_histogram.Clone("up_down_diff")
        up_down_diff.Add(down_histogram, -1)

        up_down_sum = up_histogram.Clone("up_down_sum")
        up_down_sum.Add(down_histogram)

        asymmetry_histogram = up_down_diff.Clone("asymmetry_histogram")
        asymmetry_histogram.Divide(up_down_sum)
        asymmetry_histogram.SetTitle("Asymmetry Plot; Time (#mus); Up-Down Asymmetry")
        asymmetry_histogram.GetYaxis().SetRangeUser(-0.5, 0.5)

        # up_down_diff.Draw()


        # tree.GetEntry(0)
        legend = rt.TLegend(0.7,0.65,0.9,0.9)
        legend.SetHeader("time resolution = %.3f #mus" % (time_resolution), "C")

        # Compute the muon EDM
        hbar = 6.582119569 * 10 ** -16  # eV * s
        m_mu = 105.6583755  # MeV
        p_mu = 125  # MeV
        gamma = np.sqrt(m_mu ** 2 + p_mu ** 2) / m_mu  # dimensionless

        E_f = 1.92485526994  # keV/mm
        a = 11659206 * 10 ** -10  # (g-2)/2  (from PDG)

        A=0.180

        if mode == "linear":
            func = rt.TF1('func', '[0] * x + [1]', 0, 20)

            func.SetParNames('m', 'c')
            func.SetParameters(0.38, 0)
            asymmetry_histogram.Fit('func', 'REMQ')
            legend.AddEntry(func,'y = m*x + c')
            legend.AddEntry(func,'m= %.3f #pm %.3f'%(func.GetParameter(0), func.GetParError(0)))
            legend.AddEntry(func,'c = %.3f #pm %.3f'%(func.GetParameter(1), func.GetParError(1)))
            P_0 = 1  # initial polarization
            slope = func.GetParameter("m") * 10 ** 6  # s^-1

            beta = np.sqrt(1-1/gamma**2)
            B = 3  # T
            precession_freq = slope/P_0/A

        else:
            func = rt.TF1('func', '[0]*sin([1]*x)', 0, 25)
            func.SetParNames('A_{edm}', '#omega_{e}', '#phi_{e}')
            func.SetParameters(0.2, 0.38, 1)
            asymmetry_histogram.Draw()

            asymmetry_histogram.Fit('func', 'REMQ')

            # tree.GetEntry(0)
            asymmetry_histogram.SetTitle("Asymmetry Plot; Time (#mus); Up-Down Asymmetry")
            legend = rt.TLegend(0.7, 0.65, 0.9, 0.9)
            legend.SetHeader("time resolution = %.3f #mus" % (time_resolution), "C")

            legend.AddEntry(func, 'y = A_{edm} sin(#omega_{e}x + #phi_{e})')
            legend.AddEntry(func, 'A_{edm} = %.3f #pm %.3f' % (func.GetParameter(0), func.GetParError(0)))
            legend.AddEntry(func, '#omega_{e} = %.3f #pm %.3f' % (func.GetParameter(1), func.GetParError(1)))
            legend.AddEntry(func, '#phi_{e} = %.3f #pm %.3f' % (func.GetParameter(2), func.GetParError(2)))

            precession_freq = func.GetParameter("#omega_{e}") * 10 ** 6  # s^-1
        mu_EDM = precession_freq * hbar / 2. * a * gamma**2 / E_f  # s**-1 * eV * s / (keV/mm)
        mu_EDM = mu_EDM / 10000  # e cm

        canvas1.Draw()

        legend.Draw()
        print(time_resolution)
        if save_asymmetry_graphs:
            canvas1.SaveAs("example_graphs/asymmetry_plot_timeresolution_" + str(run_number) +"_"+str(int(time_resolution*1000))+"ns.png")

        mu_EDM_error = (mu_EDM - actual_EDM) / (actual_EDM) * 100
        print("muon electric dipole moment: ", mu_EDM)
        print("error: ", mu_EDM_error, "%")

        muEDMs.append(mu_EDM)
        muEDMerrors.append(mu_EDM_error)

        del canvas1
        del asymmetry_histogram
        del up_down_sum
        del up_down_diff
        del up_histogram
        del down_histogram
        del func
    fig = plt.figure(figsize=(15,9))
    plt.subplot(1, 1, 1)
    plt.grid(which="both")
    plt.loglog(resolutions, np.abs(muEDMerrors))
    plt.xlabel("Time Resolution ($\mu$s)")
    plt.ylabel("Muon EDM Error (%)")
    plt.savefig("graphs/time_analysis_"+str(run_number)+"_" + exact_label + ".png")