# muEDM

This is a repository for simulation codes for the muon EDM experiment employing the frozen-spin method. It is based on the musrSim package developed for PSI's Low Energy Muon (LEM) Beamline.

# Pre-requisite:

1) GEANT4 installation (tested on geant4.10.03.p03)
2) ROOT installation (tested on version 6.14.04)
3) Visualisation plugin (eg. view3dscene: https://castle-engine.io/view3dscene.php, Paraview: https://www.paraview.org/download/)

# New Features:

In addition to all the existing musrSim classes, several ad-hoc classes like the following are added:

1) musrGradientField - This will enable the user to set a gradient magnetic field configuration in the simulation
2) musrRadialField - This will enable the user to set a radial electric field configuration in the simulation
3) run/electrodeEField.txt - This will enable the user to use a radial electric field configuration simulated using COMSOL
4) /musr/command/ edmLimit [factor] [power] - This will enable the user to tune the muEDM size (in e•cm) in a macro file

For more details, please check note_simulations.txt

# Running the example:

1) Compile muEDM package
2) Go to the run folder
3) Execute the command something like `../bin/Darwin-clang/musrSim 101.mac` (for Mac) or it would be a bit easier if the user add an alias directly in one's .zshrc or .bashrc file

# Tips

1) musrSim manual is available at https://www.psi.ch/sites/default/files/import/lmu/DevGeant4SimulationEN/musrsim_G4.10_2.pdf
2) For installation help, please see https://www.psi.ch/sites/default/files/import/lmu/DevGeant4SimulationEN/musrSimTalk.pdf

